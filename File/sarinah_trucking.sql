-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: sarinah_trucking
-- ------------------------------------------------------
-- Server version	5.7.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cabang` varchar(50) DEFAULT NULL,
  `nama` varchar(250) DEFAULT NULL,
  `alamat` varchar(250) DEFAULT NULL,
  `kota` varchar(250) DEFAULT NULL,
  `logo` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'00001/01','Sarinah Trucking','Perak','Surabaya',NULL);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_invoice_detail`
--

DROP TABLE IF EXISTS `data_invoice_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_invoice_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_invoice_header_id` int(11) NOT NULL,
  `data_muatan_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_data_grip_detail_data_grip_header_idx` (`data_invoice_header_id`),
  KEY `fk_data_grip_detail_data_muatan1_idx` (`data_muatan_id`),
  CONSTRAINT `fk_data_grip_detail_data_grip_header` FOREIGN KEY (`data_invoice_header_id`) REFERENCES `data_invoice_header` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_data_grip_detail_data_muatan1` FOREIGN KEY (`data_muatan_id`) REFERENCES `data_muatan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_invoice_detail`
--

LOCK TABLES `data_invoice_detail` WRITE;
/*!40000 ALTER TABLE `data_invoice_detail` DISABLE KEYS */;
INSERT INTO `data_invoice_detail` VALUES (1,3,1),(2,3,2),(3,4,12),(4,4,15),(5,5,15),(6,5,17);
/*!40000 ALTER TABLE `data_invoice_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_invoice_header`
--

DROP TABLE IF EXISTS `data_invoice_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_invoice_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_invoice` varchar(20) DEFAULT NULL,
  `no_trip` varchar(255) DEFAULT NULL,
  `no_pol` varchar(20) DEFAULT NULL,
  `nilai_awal_total` int(11) DEFAULT NULL,
  `ppn` int(11) DEFAULT NULL,
  `kepada` varchar(255) DEFAULT NULL,
  `penyewa` varchar(255) DEFAULT NULL,
  `keterangan` varchar(2000) DEFAULT NULL,
  `alamat` varchar(1000) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `checker` varchar(100) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `users_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_data_grip_header_users1_idx` (`users_id`),
  CONSTRAINT `fk_data_grip_header_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_invoice_header`
--

LOCK TABLES `data_invoice_header` WRITE;
/*!40000 ALTER TABLE `data_invoice_header` DISABLE KEYS */;
INSERT INTO `data_invoice_header` VALUES (3,'INV001','L1,L2,',NULL,1600000,0,'PT. WILMAR NABATI INDONESIA.','Ramsey','test','Gedung B & G Lantai 9, Jalan Putri Hijau No. 10 Kesawan, Medan Barat Medan - Sumatera Utara 20111','2020-03-02 00:00:00',NULL,NULL,0,1,'2020-03-01 23:41:46','2020-03-02 00:42:07',NULL),(4,'INV002','L3,L4,',NULL,8200000,100000,'PT. TEST','Aamir','Test','Test','2020-03-04 00:00:00',NULL,NULL,0,1,'2020-03-04 09:17:47','2020-03-04 09:17:47',NULL),(5,'INV003','L4,L5,',NULL,5500000,550000,'PT. Test','HARTONO','TESSSSSSSS','JL. Belimbing No.17','2020-03-07 00:00:00',NULL,NULL,1,1,'2020-03-07 04:07:13','2020-03-07 04:07:13',NULL);
/*!40000 ALTER TABLE `data_invoice_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_invoice_print`
--

DROP TABLE IF EXISTS `data_invoice_print`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_invoice_print` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_invoice_header_id` int(11) NOT NULL,
  `approve` int(11) NOT NULL DEFAULT '0',
  `deskripsi` varchar(2000) DEFAULT NULL,
  `users_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_data_grip_print_data_grip_header1_idx` (`data_invoice_header_id`),
  KEY `fk_data_grip_print_users1_idx` (`users_id`),
  CONSTRAINT `fk_data_grip_print_data_grip_header1` FOREIGN KEY (`data_invoice_header_id`) REFERENCES `data_invoice_header` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_data_grip_print_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_invoice_print`
--

LOCK TABLES `data_invoice_print` WRITE;
/*!40000 ALTER TABLE `data_invoice_print` DISABLE KEYS */;
INSERT INTO `data_invoice_print` VALUES (1,3,0,NULL,1,'2020-03-04 03:09:46','2020-03-04 03:09:46',NULL),(2,4,0,NULL,1,'2020-03-04 09:21:25','2020-03-04 09:21:25',NULL),(3,5,0,NULL,1,'2020-03-07 04:45:45','2020-03-07 04:45:45',NULL);
/*!40000 ALTER TABLE `data_invoice_print` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_muatan`
--

DROP TABLE IF EXISTS `data_muatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_muatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_data` varchar(100) DEFAULT NULL,
  `no_trip` varchar(50) DEFAULT NULL,
  `penerima` varchar(255) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `jenis_biaya` varchar(200) DEFAULT NULL,
  `no_pol` varchar(20) DEFAULT NULL,
  `tanggal_mati_nopol` datetime DEFAULT NULL,
  `penyewa` varchar(255) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `nilai_awal` int(11) DEFAULT NULL,
  `muatan` varchar(255) DEFAULT NULL,
  `asal` varchar(255) DEFAULT NULL,
  `tujuan` varchar(255) DEFAULT NULL,
  `keterangan` varchar(2000) DEFAULT NULL,
  `flag` int(11) NOT NULL DEFAULT '0',
  `users_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_data_muatan_users1_idx` (`users_id`),
  CONSTRAINT `fk_data_muatan_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_muatan`
--

LOCK TABLES `data_muatan` WRITE;
/*!40000 ALTER TABLE `data_muatan` DISABLE KEYS */;
INSERT INTO `data_muatan` VALUES (1,'MUATAN_LUAR','L1','Aaron',1230000,'Muatan Luar','AG123456UJ',NULL,'Aamir','2020-03-01 00:00:00',1500000,'Pasir','Surabaya','Kediri',NULL,1,1,'2020-03-01 05:35:36','2020-03-01 05:35:36',NULL),(2,'MUATAN_LUAR','L2','Gordon',1000000,'Muatan Luar','AG123456UJ',NULL,'Ramsey','2020-03-01 00:00:00',100000,'Semen','Surabaya','Gempol',NULL,1,1,'2020-03-01 05:36:36','2020-03-01 05:36:36',NULL),(3,'MUATAN_LUAR','L3',NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-01 00:00:00',NULL,NULL,NULL,NULL,NULL,0,1,'2020-03-01 06:03:32','2020-03-01 06:32:57','2020-03-01 06:32:57'),(4,'MUATAN_LUAR','L4','test',NULL,NULL,NULL,NULL,NULL,'2020-03-01 00:00:00',NULL,NULL,NULL,NULL,NULL,0,1,'2020-03-01 06:18:20','2020-03-01 06:32:27','2020-03-01 06:32:27'),(5,'MUATAN_LUAR','L3','test',NULL,NULL,NULL,NULL,NULL,'2020-03-01 00:00:00',NULL,NULL,NULL,NULL,NULL,0,1,'2020-03-01 06:33:07','2020-03-01 06:33:13','2020-03-01 06:33:13'),(6,'MUATAN_LUAR','L3','test',NULL,NULL,NULL,NULL,NULL,'2020-03-01 00:00:00',NULL,NULL,NULL,NULL,NULL,0,1,'2020-03-01 06:41:30','2020-03-01 06:41:34','2020-03-01 06:41:34'),(7,'MUATAN_DALAM','D1','test',NULL,NULL,NULL,NULL,NULL,'2020-03-01 00:00:00',NULL,NULL,NULL,NULL,NULL,0,1,'2020-03-01 06:46:31','2020-03-01 06:49:02','2020-03-01 06:49:02'),(8,'MUATAN_LUAR','L3','test',NULL,NULL,NULL,NULL,NULL,'2020-03-01 00:00:00',NULL,NULL,NULL,NULL,NULL,0,1,'2020-03-01 06:48:15','2020-03-01 06:48:19','2020-03-01 06:48:19'),(9,'MUATAN_DALAM','D1','Ari',5500000,'Muatan Dalam','S123456US',NULL,'Ramsey','2020-03-01 00:00:00',5000000,'Kerikil','Surabaya','Madiun',NULL,0,1,'2020-03-01 06:52:01','2020-03-01 06:52:01',NULL),(10,'BIAYA_UMUM',NULL,'Bayu',1000000,'Spare Part','AG123456UJ',NULL,NULL,'2020-03-01 00:00:00',NULL,NULL,NULL,NULL,NULL,0,1,'2020-03-01 07:31:34','2020-03-01 07:31:34',NULL),(11,'BIAYA_UMUM',NULL,'test',5500000,'Ban','S123456US',NULL,NULL,'2020-03-01 00:00:00',NULL,NULL,NULL,NULL,'testes',0,1,'2020-03-01 07:36:30','2020-03-01 07:36:30',NULL),(12,'MUATAN_LUAR','L3','test',5200000,'Muatan Luar','test',NULL,'test','2020-03-02 00:00:00',5200000,'test','test','test',NULL,1,1,'2020-03-02 01:22:23','2020-03-02 01:22:23',NULL),(13,'BIAYA_KIR',NULL,'test',1000000,'KIR','AG123456UJ','2022-12-31 00:00:00',NULL,'2020-03-04 00:00:00',NULL,NULL,NULL,NULL,NULL,0,1,'2020-03-04 07:14:56','2020-03-04 07:14:56',NULL),(14,'BIAYA_KIR',NULL,'test',1500000,'STNK','AG123456UJ','2020-03-31 00:00:00',NULL,'2020-03-04 00:00:00',NULL,NULL,NULL,NULL,NULL,0,1,'2020-03-04 09:05:32','2020-03-04 09:05:32',NULL),(15,'MUATAN_LUAR','L4','test',1500000,'Muatan Luar','AG123456UJ',NULL,'Aamir','2020-03-04 00:00:00',3000000,'Pasir','Surabaya','Madiun',NULL,1,1,'2020-03-04 09:13:49','2020-03-04 09:13:49',NULL),(17,'MUATAN_LUAR','L5','Ari',1500000,'Muatan Luar','S123456JU',NULL,'HARTONO','2020-03-06 00:00:00',2500000,'Semen',NULL,NULL,NULL,1,1,'2020-03-05 12:59:02','2020-03-05 12:59:02',NULL),(18,'MUATAN_DALAM','D2','Test',4500000,'Muatan Dalam','S123456JU',NULL,'Test','2020-03-06 00:00:00',5000000,'test',NULL,NULL,NULL,0,1,'2020-03-05 13:41:48','2020-03-05 13:41:48',NULL),(19,'BIAYA_UMUM','D3','Dudud',5000000,'Muatan Luar','A123456JU',NULL,NULL,'2020-03-07 00:00:00',NULL,NULL,NULL,NULL,'TESSSSTISSSSS',0,1,'2020-03-07 04:52:43','2020-03-07 04:52:43',NULL),(20,'BIAYA_UMUM','D4','Test',1122222,'Lain-lain','323232',NULL,NULL,'2020-03-07 00:00:00',NULL,NULL,NULL,NULL,'test',0,1,'2020-03-07 05:06:21','2020-03-07 05:06:21',NULL),(21,'BIAYA_KIR',NULL,'test34',5555555,'KIR','test','2021-06-08 00:00:00',NULL,'2020-03-07 00:00:00',NULL,NULL,NULL,NULL,NULL,0,1,'2020-03-07 05:06:52','2020-03-07 05:06:52',NULL),(22,'MUATAN_LUAR','L6','BUDIS',4566666,'Muatan Luar','232ddsd12',NULL,'TREEE','2020-03-07 00:00:00',43434334,'Kerikil',NULL,NULL,NULL,0,1,'2020-03-07 05:10:04','2020-03-07 05:10:04',NULL);
/*!40000 ALTER TABLE `data_muatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modul`
--

DROP TABLE IF EXISTS `modul`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `modul` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modul` varchar(100) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_hidden` int(11) NOT NULL DEFAULT '0',
  `icon_class` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `modul_modul_uindex` (`modul`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modul`
--

LOCK TABLES `modul` WRITE;
/*!40000 ALTER TABLE `modul` DISABLE KEYS */;
INSERT INTO `modul` VALUES (1,'Home','home',NULL,0,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(2,'Sistem','#',NULL,0,'fa fa-cog','2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(3,'Inputan','#',NULL,0,'fa fa-server','2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(4,'Invoice','#',NULL,0,'fa fa-file-contract','2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(5,'Laporan','#',NULL,0,'fa fa-chart-bar','2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(6,'Muatan Luar Detail','muatan_luar.detail',3,1,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(7,'Muatan Luar List','muatan_luar.list',3,0,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(8,'Muatan Dalam Detail','muatan_dalam.detail',3,1,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(9,'Muatan Dalam List','muatan_dalam.list',3,0,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(10,'Biaya Umum Detail','biaya_umum.detail',3,1,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(11,'Biaya Umum List','biaya_umum.list',3,0,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(12,'Biaya Kir Detail','biaya_kir.detail',3,1,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(13,'Biaya Kir List','biaya_kir.list',3,0,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(14,'Data Trip Detail','data_trip.detail',4,1,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(15,'Data Trip List','data_trip.list',4,0,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(16,'Kwitansi Add','kwitansi.add',4,1,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(17,'Kwitansi Detail','kwitansi.detail',4,1,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(18,'Kwitansi List','kwitansi.list',4,0,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(19,'Approve Detail','approve.detail',4,1,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(20,'Approve List','approve.list',4,0,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(21,'Olap Detail','olap.detail',30,1,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(22,'Olap List','olap.list',30,0,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(26,'User Detail','user.detail',2,1,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(27,'User List','user.list',2,0,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(28,'Role User Detail','role_user.detail',2,1,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(29,'Role User List','role_user.list',2,0,NULL,'2020-03-07 13:09:44','2020-03-07 13:09:44',NULL),(30,'Olap','#',2,0,NULL,'2020-03-07 13:13:08','2020-03-07 13:13:08',NULL);
/*!40000 ALTER TABLE `modul` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modul_access`
--

DROP TABLE IF EXISTS `modul_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `modul_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access` varchar(200) NOT NULL,
  `modul_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `modul_access_modul_id_fk` (`modul_id`),
  CONSTRAINT `modul_access_modul_id_fk` FOREIGN KEY (`modul_id`) REFERENCES `modul` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modul_access`
--

LOCK TABLES `modul_access` WRITE;
/*!40000 ALTER TABLE `modul_access` DISABLE KEYS */;
INSERT INTO `modul_access` VALUES (1,'CREATE',7),(2,'DELETE',7),(3,'CREATE',9),(4,'DELETE',9),(5,'CREATE',11),(6,'DELETE',11),(7,'CREATE',13),(8,'DELETE',13),(9,'CREATE',15),(10,'DELETE',15),(11,'CREATE',18),(12,'DELETE',18),(13,'APPROVE',20),(14,'CREATE',27),(15,'DELETE',27),(16,'CREATE',29),(17,'EDIT',29),(18,'DELETE',29);
/*!40000 ALTER TABLE `modul_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pelunasan`
--

DROP TABLE IF EXISTS `pelunasan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pelunasan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_invoice_header_id` int(11) NOT NULL,
  `no_kwitansi` varchar(45) DEFAULT NULL,
  `jumlah_tagihan` int(11) DEFAULT NULL,
  `penyewa` varchar(255) DEFAULT NULL,
  `tanggal_bayar` datetime DEFAULT NULL,
  `nominal_dibayar` int(11) DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `audit` int(11) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pelunasan_users1_idx` (`users_id`),
  KEY `fk_pelunasan_data_grip_header1_idx` (`data_invoice_header_id`),
  CONSTRAINT `fk_pelunasan_data_grip_header1` FOREIGN KEY (`data_invoice_header_id`) REFERENCES `data_invoice_header` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pelunasan_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pelunasan`
--

LOCK TABLES `pelunasan` WRITE;
/*!40000 ALTER TABLE `pelunasan` DISABLE KEYS */;
INSERT INTO `pelunasan` VALUES (1,3,'INV001',1600000,'Ramsey','2020-03-05 00:00:00',1000000,'MANDIRI SRI',0,'Partial',1);
/*!40000 ALTER TABLE `pelunasan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `print_out`
--

DROP TABLE IF EXISTS `print_out`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `print_out` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `design` text,
  `design_def` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `print_out`
--

LOCK TABLES `print_out` WRITE;
/*!40000 ALTER TABLE `print_out` DISABLE KEYS */;
INSERT INTO `print_out` VALUES (1,'PrintTest',NULL,NULL,'2020-03-05 10:15:01','2020-03-05 10:15:01'),(2,'Invoice','{\"ReportVersion\":\"2020.1.1\",\"ReportGuid\":\"4f40db274696d667f4ba371eccc6427c\",\"ReportName\":\"Report\",\"ReportAlias\":\"Report\",\"ReportFile\":\"Report.mrt\",\"ReportCreated\":\"/Date(1584076314000+0700)/\",\"ReportChanged\":\"/Date(1584076314000+0700)/\",\"EngineVersion\":\"EngineV2\",\"CalculationMode\":\"Interpretation\",\"ReportUnit\":\"Centimeters\",\"PreviewSettings\":268435455,\"Dictionary\":{\"DataSources\":{\"0\":{\"Ident\":\"StiDataTableSource\",\"Name\":\"company\",\"Alias\":\"company\",\"Columns\":{\"0\":{\"Name\":\"id\",\"Index\":-1,\"NameInSource\":\"id\",\"Alias\":\"id\",\"Type\":\"System.Decimal\"},\"1\":{\"Name\":\"cabang\",\"Index\":-1,\"NameInSource\":\"cabang\",\"Alias\":\"cabang\",\"Type\":\"System.String\"},\"2\":{\"Name\":\"nama\",\"Index\":-1,\"NameInSource\":\"nama\",\"Alias\":\"nama\",\"Type\":\"System.String\"},\"3\":{\"Name\":\"alamat\",\"Index\":-1,\"NameInSource\":\"alamat\",\"Alias\":\"alamat\",\"Type\":\"System.String\"},\"4\":{\"Name\":\"kota\",\"Index\":-1,\"NameInSource\":\"kota\",\"Alias\":\"kota\",\"Type\":\"System.String\"},\"5\":{\"Name\":\"logo\",\"Index\":-1,\"NameInSource\":\"logo\",\"Alias\":\"logo\",\"Type\":\"System.String\"}},\"NameInSource\":\"source.company\"},\"1\":{\"Ident\":\"StiDataTableSource\",\"Name\":\"data\",\"Alias\":\"data\",\"Columns\":{\"0\":{\"Name\":\"id\",\"Index\":-1,\"NameInSource\":\"id\",\"Alias\":\"id\",\"Type\":\"System.Decimal\"},\"1\":{\"Name\":\"no_invoice\",\"Index\":-1,\"NameInSource\":\"no_invoice\",\"Alias\":\"no_invoice\",\"Type\":\"System.String\"},\"2\":{\"Name\":\"no_trip\",\"Index\":-1,\"NameInSource\":\"no_trip\",\"Alias\":\"no_trip\",\"Type\":\"System.String\"},\"3\":{\"Name\":\"no_pol\",\"Index\":-1,\"NameInSource\":\"no_pol\",\"Alias\":\"no_pol\",\"Type\":\"System.String\"},\"4\":{\"Name\":\"nilai_awal_total\",\"Index\":-1,\"NameInSource\":\"nilai_awal_total\",\"Alias\":\"nilai_awal_total\",\"Type\":\"System.Decimal\"},\"5\":{\"Name\":\"ppn\",\"Index\":-1,\"NameInSource\":\"ppn\",\"Alias\":\"ppn\",\"Type\":\"System.Decimal\"},\"6\":{\"Name\":\"kepada\",\"Index\":-1,\"NameInSource\":\"kepada\",\"Alias\":\"kepada\",\"Type\":\"System.String\"},\"7\":{\"Name\":\"penyewa\",\"Index\":-1,\"NameInSource\":\"penyewa\",\"Alias\":\"penyewa\",\"Type\":\"System.String\"},\"8\":{\"Name\":\"keterangan\",\"Index\":-1,\"NameInSource\":\"keterangan\",\"Alias\":\"keterangan\",\"Type\":\"System.String\"},\"9\":{\"Name\":\"alamat\",\"Index\":-1,\"NameInSource\":\"alamat\",\"Alias\":\"alamat\",\"Type\":\"System.String\"},\"10\":{\"Name\":\"tanggal\",\"Index\":-1,\"NameInSource\":\"tanggal\",\"Alias\":\"tanggal\",\"Type\":\"System.String\"},\"11\":{\"Name\":\"status\",\"Index\":-1,\"NameInSource\":\"status\",\"Alias\":\"status\",\"Type\":\"System.String\"},\"12\":{\"Name\":\"checker\",\"Index\":-1,\"NameInSource\":\"checker\",\"Alias\":\"checker\",\"Type\":\"System.String\"},\"13\":{\"Name\":\"flag\",\"Index\":-1,\"NameInSource\":\"flag\",\"Alias\":\"flag\",\"Type\":\"System.Decimal\"},\"14\":{\"Name\":\"users_id\",\"Index\":-1,\"NameInSource\":\"users_id\",\"Alias\":\"users_id\",\"Type\":\"System.Decimal\"},\"15\":{\"Name\":\"created_at\",\"Index\":-1,\"NameInSource\":\"created_at\",\"Alias\":\"created_at\",\"Type\":\"System.String\"},\"16\":{\"Name\":\"updated_at\",\"Index\":-1,\"NameInSource\":\"updated_at\",\"Alias\":\"updated_at\",\"Type\":\"System.String\"},\"17\":{\"Name\":\"deleted_at\",\"Index\":-1,\"NameInSource\":\"deleted_at\",\"Alias\":\"deleted_at\",\"Type\":\"System.String\"},\"18\":{\"Name\":\"data_invoice_header_id\",\"Index\":-1,\"NameInSource\":\"data_invoice_header_id\",\"Alias\":\"data_invoice_header_id\",\"Type\":\"System.Decimal\"},\"19\":{\"Name\":\"data_muatan_id\",\"Index\":-1,\"NameInSource\":\"data_muatan_id\",\"Alias\":\"data_muatan_id\",\"Type\":\"System.Decimal\"},\"20\":{\"Name\":\"username\",\"Index\":-1,\"NameInSource\":\"username\",\"Alias\":\"username\",\"Type\":\"System.String\"},\"21\":{\"Name\":\"password\",\"Index\":-1,\"NameInSource\":\"password\",\"Alias\":\"password\",\"Type\":\"System.String\"},\"22\":{\"Name\":\"nama\",\"Index\":-1,\"NameInSource\":\"nama\",\"Alias\":\"nama\",\"Type\":\"System.String\"}},\"NameInSource\":\"source.data\"}}},\"Pages\":{\"0\":{\"Ident\":\"StiPage\",\"Name\":\"Page1\",\"Guid\":\"1aa1713356a6ada89401e63648d10350\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Border\":\";;2;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"Components\":{\"0\":{\"Ident\":\"StiPageHeaderBand\",\"Name\":\"PageHeaderBand1\",\"ClientRectangle\":\"0,0.4,20,3\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"Components\":{\"0\":{\"Ident\":\"StiText\",\"Name\":\"Text1\",\"Guid\":\"fd9dcc0b89b91fcc9ab911fe8a8b9881\",\"ClientRectangle\":\"4.4,0.4,11.2,0.8\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"PT. SARINAH RINTAS INDAH\"},\"HorAlignment\":\"Center\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;22;Bold;\",\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:Black\",\"Type\":\"Expression\"},\"1\":{\"Ident\":\"StiText\",\"Name\":\"Text2\",\"Guid\":\"558df351a880e8975a1674bba000b032\",\"ClientRectangle\":\"3.8,1.2,12.4,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Jl. Laksda M Nasir 14, Surabaya Indonesia\"},\"HorAlignment\":\"Center\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;16;Bold;\",\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:Black\",\"Type\":\"Expression\"},\"2\":{\"Ident\":\"StiText\",\"Name\":\"Text3\",\"Guid\":\"d02a4524d1aefbd257b90ad9236db659\",\"ClientRectangle\":\"3.8,1.8,12.4,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Telp : 031-3291225 Fax : 031-3291517\"},\"HorAlignment\":\"Center\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;16;Bold;\",\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:Black\",\"Type\":\"Expression\"},\"3\":{\"Ident\":\"StiImage\",\"Name\":\"Image1\",\"Guid\":\"058e446c3a51ac0449d9a9a345d39def\",\"ClientRectangle\":\"0,0.2,2.6,2.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"ImageBytes\":\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFwAAABbCAYAAAD+6uLgAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAAJcEhZcwAADsQAAA7EAZUrDhsAACQlSURBVHhe7V0HfFbV+X7OudmDhACBEKaAgoiCCALWPVHRqnUrjrqqtT9xFG2dOKmzrrqwarX+1WqtooiislFBkFWRpWxISAgJ2fnu+T/P/b4vJIEMkJHfr756+O69Zz/nPe953nNHjKPgF9ljYiO/v8gekloaPnjIEOTlbkBKggddDEfUnQAm8huV7U2QumkkO1KO4rZXrqShuJ8j0XKj7WqsvZL603jGodIHSkp99OrdG6++8goyMjJqA56SkoqDuhbjnOMMKsod/BAv1q1nZ9oh2ZFyFMdzv04atdTwevDLcx0rTZCcx14kLsSOBumCTJFpzH+CnjIYHite4jOtxPI8uBZkokTiqyV6vabUk0ZtSk8Hlq0Gnn/PoWOPfpj4+SfIzMysDXhmmzY496g8PPwAMxXxcjkv1jU6dSuuW6mkbhpJYx2oGa84nm9tWUQi16vzRo4dfwUWlSqIig5IzWTbXAguUiKAq5/BpVqZakj0ek2pJ41jmbGdgcUzDE66yiEhsx+mTw4DXgvO2Nh4eLxCi4Ik/u7VwMYnK7AtyexYMjuRzOvBec3AaymKY/uVT3lSlD6Gv5Gg4+q80eNoqFHONm3YyaCy4lhmbBjWWlJLw9tnZWPYwHV4/k9hDS+tYIK6o7i7JdKa+MTwb1kJGx9PcxEHVPA4RDMnkyCRJsXxegzRrigCqhiXmMLrVZycZeE022jhnhC2K6kj8M03Bqfd7JCa1Q/TJm1Hw9W4AGCOjoLZC8ExqFVeGgPtoOVxLMG3rTjzIoMQTav2clLCEOTYhHBam8p8DIGdjqTbG0H4WR0HLd4qtQFvBhLDRsZwLk6bbDF1ikUcwVu+3OL9f1nkbqZG81waJMWIy+DC9JPB62Ms1uTwnAMy7kOLWdQsy4FQh5ubNDvAZfu0jjz2msGDL4c1ffIc4PejDRb8aBDbkqajkoDT9MS2Ydw8g+ufDMd51PTbnzN4/h0DJ1OzPSO6l6XZAR42Zwa5pQ7ri4WqQSl51hqe0zQTxfA0DUwfj4totwuo8uXqCc9XMl0ubX1QjtJUr1DNQ5oN4OLDAbWjmfAygc7tgG5ceJDp0IHXshIN2uucGp5Cex7HAF7nOkRsLdLb8pzXWnMB7ZRFe9+a5+zdNtRyL0uzAVwLniHoVWQbxZsNyugDhHyOQKlBSZmhM+F4jedkTqBJCQLjKgO153WlJSGX81NWwfTMw8M9z7IakWYBuOhdPDWzrMzisacsbr0fmLXUYe4PDnfcBowZ51BQ4eOJvzvcf5fByCcMbnvE4L6RBm9+QI+Y/732JvDovQbrN/uYNNdh9P0GeTlcdMlempPU5uHts3Eaefhzt4d5eNke4uECPImmIW+lRf+LgPW0wxW026o6kXa5jHY6xFbG0y7HU0Uqeaw4j/+WMZ32LJJiTBBXoEj+n8wp89UzQO9+PkrJbvaosD2JNH9iS8NGOKTUy8P3phAkmRWf7KKcIPKU7TYopskQ2JJyAl9IU1LKayUMRVUCOzAcPHfYVMF8YbxRJbbDwVLnm5PsHsAjAO2QEBgtnBkpWzPXN7vCEG8N25MOdJriZU44SM1Jmo+GEzkpo6sBUA1rt11RbHgubJuugjOhilrfjHoYyO5pTn1q15AQmxiagDjy7sZk+xDXFu0gNjdKKGlW4y+otQDuCqlSObJJzQz05gM4W6LdvkJ5iTVEeyu6OVCfCNNYzoy6aQrI5yvE2f8nTMrOSGDASU3TDNLoVXZvbzCol8XA/SxaakO8Hokn2H26WAze36JnBw/7ZHr0THmcbZGgvZRfFs1tRVuplVvIXeMd7rvG4dXbDd59BHj2FoPzDrNIia8f8MqQwcFdPDx4pYd3H3d4Y5TDSyOZ948hZLbxURHdF28m0mw0vJKuvOc5HHGUj9Mv8NE+yWLsZId/TvSRU1S/YQ+Rs0+YG8KYj3x4HLRBp/o48TQfgwY4JCaGzVRzkmYDeLC+ERzdQPh+usVF99BVf9vHV0voKcoWNyA/5fp44/MQLr7P4V9/p9GPLJZV2mepf3LsFWk2gItMxCbRg1xjMXK0wfg5PjbXWUAbErGSmcscRjwMfDmWKNO2B3d9/hdYys7y3xgumBOnAWPnRVbQBiQloCXbqu/qQh9/ewsoLjaI4wA2N2k2Gq59FN3mnrFQMNYPtijgFacbPPUXg8tPNGEmUkscpn8P5K1jmbrf2Wx6GJbd0pz69kAakuisaJXW8H5TaozBtedYXHqTwx9/65C8HZIe3LzV5WZmTiTNZvz1lJcrdTh2kEG71PqbJdYxbhLZyWaDth0MMrZjNo49hNfbMw0pYXNz75sN4AEXL6ATc6DDlcMsYutpmbZnH3mJdPEhi/J8i7OHGvTZx6ClHKYkg17tLa75DZDMgagoZYZfAN++yARUiv75Pm6mqTjlEK8el95hE9Nc+bCPl/4BDKcdf/EOOj63Gtx5tcXD1wMDDgghVBjeG/+FFjYgsr1lm4AWmSGMvonAdeUKWQ9i5SEfD70fwv3U9vg4hysuA669N4QTz/Phc+BKiyN2vJnJzwZ8l85YAkTHERU5wL4H+fjrn4CDu9XfxC3lDm9N83Hen3w8dY9B3myDmEQG2u+4ZJZF4HX7rjnJ3tdwgqyp79M2K8jb1DOEfp7DoceEMGxgJF09UkFAf1jhcM8LPoZfbvDO4xbF+eTg+wJJHZiA8cFj181EfjbgwazdWTVn5lA5fwhKvG6JtWSgax8TR5BoWkK5DsVbHJLiDY7Yl/amASkIOXyx1OHGZ3zceJfFrPEeTKpB0j4InuRqLqDvNQ3XGIXIIuTOJ3YjyB3DpiCmC5DQnb88LlxjMGkW0Jcs5NknDYafouY2ZJgdVnOAXpjg45q7+fuERWWhRQLLVK7mQBH3DuDsuKP5iKeTE98V2JxrMOM9i4lvW8wcb7BmKeHJopOTCCyix9itHdD7xBAefcDg/COl6Q2BLnH4dhU1/WmH+0YblBVQ01mGnpXb25jvGsAb638d8dnxmARqN0H44WuLu+82uOwuHxePAn57u8GIOww+I/izf7AoIkTJiYRpEdCa7OXZ0Q7XneYhoQk+ezEN+Kj3fDz5V6ZNMIhNCde9N2WPa7imtSXbiyfYS2Z6uOpOgycmOPxA3ry62Mf8HId3viHNuxO48W+G1M6hXRtmpNdYtRBIbxvCI/f6uPYMi9SEpjTf4f53fHz+MZ2pTA40J8je1PI9DrhYQ0Irgldi8MALBpOXqPs1IQifr9zsY9oPISTHGXTNYjNjHSq4wJYt1oP51Nw7wqDHiao3IoVVDne/wLwbTLAw13wUY0/LHgU80G5tLJEjL/rO4rPZ6nlDRNkhkYylBxdQ3ZvUow965aRsGV33dB+3XuswKGAvjdu0mUt9jJ9oYej+76AF3KWyxzU8+lbCEnLnjfQGGxOP7mIKBwgEPrgjxPwh2mGfi2l6Zx9HHRhO15iUc1w/nMIDcnw9kbW3GMueNykSdraqqml6VlDiME5AcdFLpC0PnCNNCg0cKWVKZAAbF4eZi31szjHwmG9veaB7B3DWqrfSmrLXUUb7+/R/gM9JG71ONCXk7Cn0Im1nYOkXHt6bqlRNU9c88n69JxS8z/e/oOECWDZYtXbp5JBBr7IpsmoTOfVjwJjHPcz+ymDRPIv3XvZw3T0OX61ouqrSgjdpkd2dssc1PLjRsAXYvy89yC5Nrd5h3hofIx4hB78PuHaUwR8ecPh0oUbPcSCbZp4yWwBtM3igN6z30sq5xwEXNpX5ooYOw0/lAtbQc2y1xKGIhverZQ5fEug1W8Ka3VSwhfCxh7C+TEdKyrM93vOw7HnAWaMe+qnKdTjjLIezDv95TdAjzY091izJamGCAYZ1Qf1NHqddLHtnnMksynPJ9FIdbrwC6JrRZKqxUyJsLzkJ6E0KWbWRg7SXtFvS9KqlRDUVqeZ5jWMpW2Mh0C6GytUO/Qc43Ha5bgbvHhTk5hx9oMUNl3H9IBkvj5oTtqPJoTFpSpqINLmX1e+RM8f2gkrSr96r8Ui7gt8GgjavQrqHSRf+sotDuPlCg6x0vSa16+a65cgO2M/D/TcYZLb1UZani8SH5n97ilBvUPqGQiRdU0RQNShiFfqAQAKdjiC0joSa5/yVU6Lj+CCY8C+vxbfhsUJwruNImlYGCdoyFfhpDtffHMJ9fzDYP3tXgK73NS1OGeTh2T85DOwbQiXBtlQE3XrTHryCjqPnekpLH1HQb/S6XmXUNU9fs4gEKUp1iJwrv5RMmt4Y8I2+NijxnAneOw1RY6IFB09KSTjCWog8an9luUEFPUPfl7qTrgVptOOngWMmHehhP+XhdcdjkZQkAh63H6+nkmN/YfHym8DYSQ5L1tDm1pqvNY/rigZJrxpadM+2OPt4h2vOBDp2Y2WlzEdHS95qICqmrqpFiw6S8CQ4j6SP/FSnkYSrC/+qrC0OpWyv7skKm/peG6wXcD1mUF7FNrYEls+yeO4fdLNZuL5jokJjeKy3fvW5I0vA4zkQRaUGhUU+QlUWwtwSUKe3g3msN9SEc2C/WaVeCVTNccyXzRkxoBdw7Mk+Wh3uULKJwH9i8dk3DrMWGixYFvYQS2WDKwxCtXrOMjjY6SkWB9D7PGqww9BBBj16+kjLYjol3WJQTA3PXWuxfl24LVq4KyvD4OgxaUeNkqmrYucq9K4n01AdkJZCPWBQu2U+9FaF+iyQta+j/Z2iMqBTho9s1hdiuiqmSaJXvMOAl7FByezE9PEWh92gFAFkDMqiX0l19u1ING1dqXvdIDvRoGe2wZW/8XHuNbyUzY4vMsghq1i90iA/12D9JmATZ55eBa+gMlRW+RxMi/atgH3Zzk50+7t29RGn4qkE4JQvXGAwforBR1MdlucYbObABd/EIlh6ikszLKoYAj9EOKo4G1WET1STaTIStA3BcwGuPAFiTBtWHioCcTpsH+CZhzjTE9i+DcSt6w4CHnwRiCOXnA0s+t7g+N9ZbChUB5mwkiaGNQXaEcPs/PWrDHQojbWRfRKPU0CNLSzmyLOxsYpj2koCRmVCQjwXMgKXUxRUTzFow/hrhlr8+S4f8W1ZYCEvs9NBl+UhVrEsqpHKC2YXLwVfD6IdDfRhI+MJgJdpMHeCxROvO4yf77COpq62CK2oKC6CXi2peW17x9EyHLq29PD9WLY52aF4JXHjAOw44OygtjIrCPDEry2WrGe/4jiKyQZxsrvUoNhYZme9PkFQQXoWxIp6CWDaTH3Fp6BAWsPB0MLD3wpO3wSCmUZvU49ErPpR78yzgf+NPhNucO9pFrffJ0RZqoDUCCnIFlM7q0Vxum2mx9qKNZistz2wdLKHS+7i7FzNCinpqUCvzpwNnOopSQ659Hb10JHWJ7BPQSc4goYaFTAPZlPfymlu4jllEtVX2lGhVUmzU0ql0RMFFYX8Zb3HH2rwyI3KTw1n2clddgJwmRRVLC319HkMzdXWTE6vLeik9rNpt6V1kJalMUgbBVqeWsbfABymUS3qu+5PkqnoOygoYdjM800W86ca3Pikj88Xau7IGhh8/bRFnzPY+xxpMq8yue5CCJBgOmsBDg4kkfhMToqfLC4ebvDBkjDYuq128eEerj7DYL/eBIODbTwf+WxnGU2WkeepRYlliCFphmhglU7uQVmhhSG4JeyryorjgJlErlMJIeRzdq5bZtC5hUN7YqMXA7RBp29e7RzgLEBKlpjOBLSTerd93RyDaTOAJWuJeRHNCxeZBGpQF3b2SDoyHQ6SZhM4zgg9warntFWOR5olQHJXA/MmWkyjfd1IwLXodkoz+Phb4Mvv/WBRlhzT3cNlZzA/Z43K073QynIbLEoBn4/VNRs2c1wJA+WgYkz5zOL+j1hhMHS8xsEdfrjFgO6c7sS1NRfpw/s6ZA5iXCumYz8C5dHskSKpPM6SEAFeO9NgFvu7cpVDPtc1kYM0sqluHSwOGuwjqx/zS8kWsQj2N/BLeLrDLCVKC4NxZ5l6zECL1ftvWPx9LDB7BTUknK1aUpm6Nxt66bEGF1zsIzWDHWQj1G/Z9URO543zLe54EviU4C4XXQsk/CslE3usbhDLk03XrI/af2mPguVqJ6ANB8to5Yvgq6+8reAg1mUyScxfIs2lxLHc/WnjTx5ocNO1DhlcaCtXcTAZrzrkHxTTVLz6MinqBOC/a3xarNrlteDMOrCLxWXHA+dd7QemtphlaMIJs/oAp03aKllZ7d3Vp3MMZhoX+gKu+BO4LR/DVc2k8vDaQ+fHuI7xnrYiVLszxjiPS7wCKwpfY2gTY9zNJ3mubIJxVdNZzliO2Vzjlv3Tc2cd4LlY0YJIGcrbJsO42FiVE75WM6g8rr2OmIZD5Bqn/tZrNYLi6pah6zFeuJ30PqvLjeO1sw723LJ3rXML4Yr+w75OZV+/Me7uMzzXKsZWl6d8yYnGJcRvLUNtaRfruYfP91xoNvv6FfFiX0vGEdYFcDNfNq5dGlz3nv3chg0bAowb1PASLkSJ8qLoGb75qsXvnqbJLZMqceFJ4HQfCvTt6VBJjVu6wuCfnzqs3RSOT+Y0HnUR7fIN1LZyLjKlFrfcZjBmOr2+IAWndW+L80926MJZsWkLZw0595gPHApKwmVI+nY1OKKfxeqcsIlTXfoKkBwtaWMg7II4cQrXmHnL6DCtre4ShVo2xOJompAErh/rSC//Q5o470cWRNFScuUxXPAe1LELHrN78xXa+2ccivTgIuWATh7OPwHo3oWziekXLaX2jwOWrQ+XkR5v8eINBr+5JIQK8nwt3KLTO6zh0m43x7iV1ID9WsmSh0c7q4V1L97huY2zrKtkfMUs4/KnW/fvhzy3T7ut6Xq0Zt73qT25cJNHW5ek+R/EGXfCQZ6b9Z7nyr9nfdR+n+Xkz7RuzMgYl5awtYzhQ6wrYNlLPzbu+3eMW/C2cXP+z7h5/F2g8+Aa3IJ/w62YZN21Q7fmVT03nGnd9+OtK6XWlnOmbmbfJr3huVP6k04wXumSOCM/GuU5lwe38TPjhnTaWkavjtZ98pzntrCv/nfhthbNse6Dp2Lc/llbyxiSbd26cUpD3D4kbvO3r+FRHdlGpPcabd3/m8aF4wexDopW8d/92uDy4Q6t0nzEVDjEcpVrmezj12QUt15gaM+oBpRl5MQz5vKANmHKLM4YLQaUjq0M7rjKof8QOillLJcMwNCet+TCeOmFdHyOZd20A5Il66l1XEu69XHo2c2h934OfXs59OFv730jobu+/EPHhw7QhhoLy5CeFndeC+bzkUBsxP5axIdfvn1wBPN0CNehj9t8MpEHVN/Z9Dlmrwz3VW248RyDE4/zOWNJ+dRW8vmUOB/DTgkFcXERBKevoVdMnAwtQvD5vnAR20i9gCtDsG9CjPQ4cLSENE7ds4dyCif4KNHKTFpVSre5hAuG6OHldM+j7+gox+LV/LeA4NP7isqBdKZ+dTgL3uSw5UfmpfdXTE5cvIQNSvdx9jFc2MS5KQvXADOm84Amp4L1VOVuGypUNineYi7Es1hGWAxOHwy07MEFcTmjmSZo5wpG0Tz1IehDOUhRWbxe7TH4kYs8LUIgooRnHsb2E2g5MyXMv4WhjG3W6n4qB6J14JRJHJb9xDL4vwjCjgNOCaw7lSAhorESlof1q3lO4GP1qdJIVIx4OCvfRADKI7ZP2bUtIRc7ia57VFay0XnLWDXz6+uassexbKR2HbVLtl6bVuEiUEEzGXxalPZbT16VU9u3CVoUGCdDUBpUGJZicfwKspkWLIPVaQ8lKEv0lPywgIMclYCKMk0a6a30TCL3Ygk5trxY7SkF28qMjNV9UfZ3DdctPe8SlQBolb+1CdtI/YAzRsZfgPfvS2AiyBZyZB99zWHVdxaxXeiBUVsTSaPi2hvkr/Iw6iUCxgVXEsPMfXswX4rDwOCBnXAZ8zn9Hn/OYDMdlLiOBvFZzM8yPPLjheTQz71HD069pXRqS/rFvD55ssDS4rhNIAiK70Ke3YPlhcXh7ckOM8d5sDRhamcy60ikyy3++S/S24+/3YrMfvuwwzSLA2i62smxo5Swr/f9w2EFF3MvI9zOBC7wXhuD1XMt/jLGIU9mhpJM29KP/oecuyptQdSDrHc3JXKMRx99DPtlb8GpR/CEmqOXnORZtWYlKwjOPE0lymJOybXU0NbsaCld+gJOxaX/NXj6JYtnPtNH7cJyZD8Pf7pMgyV7b/DdPIufmFei17SLV9lgC0S7jOs4a76YYHHv8w5TfgyniaXtv+VCi+OPpItN06VOROGsJbyoLYU4AuFVWXxKdlBJsDbSzCyk2UunepN2BlsVq9ZaTHjX4M4XHH6KKEZr2slRv6dfl8V1JIlgrjPBC7qSxRsccpZ7iK+iNtMPyc0xmPuVwV/+ZvDWt9GeWnqxHq44l+XROapkvXGcEWvXGLw5nsepWfjtZRchOZlTK1g6I7INSyEPL2Vw8+DmvhXjBvfS9lR4VdZvRpJ1R/e27uS+nuuRUZsdHNDVc589E+McV/Zy8dL5xk19yXM9AwawtYx2ZCSHd7PuELIBr/o6XCL57iUnkx1M5MpPbrzlI/Lb8fWHwF9gOrX9hnM8l1yD6aSRN5/cx3MXHWHdkT2sk+Mbjcsg43p0RCx5uHVVE+gvfA23mqxm6GD6C2Qv4XTGtfSsG9TZul8xtBR/jLRV/P6kwTFu+adkOfNM4HMU7ywPj+6l6M5GPKf23Nke/kh+On+xjxx5czXsl0S8uAVtWx+65DddwkWLC2N5gR9MMd09iSGf/3yShzufN1iy0if3Dm9qhfvOLlBTtV605fQ9mdz5tksdsjND4VtjnGmNidqaSKaSV2jx4BiL9yb5WJu71TzVlHhqfKe2FpeScd1M/hzLGVGifRXa6aS2NHuLPdxP8zhldggbCznha6wNkjiapbYZFofRjNx5NdCrZwjl5P/6goWsb5Nd+2ED1uH5P4cBj36wXQuKtl21WGzMs/j3p3TxpzgsXM6prgVLFcQbTkmDo2jvzz0J6LGPD8tB0TTWgqVa9JVMm84pu4JgTDSY9B1ZARtZVEx6yI62YdxB+xocP9DheH3vRNu3ZDDbwrV9UR3BoKVxDaVp+YxT/50vSPN+oK0t0LZuGKjW7MehfSwupFs+6NAQqWmkHuZVGdpi0Ldt8wo8fDDJYOJs4HuauZx8n/EGmVSIXly/hg4Bhh7hIy2FSkU6qj36wBmjImrzaiZp4rAba3+wvTbgWdk4c8g6PD2KJwTcF1isXD1WKm0eiVmU8WIB7dwKUiXdiRHLaM3VPTuThXNhSiXX1T627KryqyOSYCKKArCMEroARTkWm8nvi4rJ5VmGHn3TwpaaSn6vBZtUMGhdPQtQLVG66C/r00fcqzgzC/Itcsh6VuSQmLA9GbTRnanBLTvRXrcg/6fNddq4Uh2RdgYbdppRLKMs1mIL2Uwh156czSaIa0f7nEqnsQUHJY6MzLGvml0BVuqv8nfiLOGAn3id44zZ+jcgtgH8wmPX4eFHeaIFJcJSgqBUMiFqmGiVaGBUovEK0nh1QL81OhH8aidOZQp0beWqDM0/jUS0fJkY7W1rpde1mmBHy4qK4qMSjYuWI1H5CgKvZl5pgMyM6on2MVqP8keDrquvUboXFcWpnRzAYCs6iovSR+O6kv5ONfjVRVSm1v0wY0o9gB/YaR0uO5urfDlHPmIuaokKo+hPB8gTJZEI0sie6/0ZjXTQgBoN1N5HPqf0ZnJf7fppylbSJiZF7hdqKqaxYx7B199vCOpQByIgbKat37yFDIRlRvdPws3SDevwF5b1NrL27cOBsVHQlEf8mUF5ZB71AclCzsz8AhP0IZi5vC4kZIP17a3WVAh51UEZrFO2XfVL9Hx60M9IOwOTGcREhP1P5Xq1dInB/WN8tO3SF5O/HL8t4J06dkJ+7mpkpOhGbaSyeiTw0hWvX/WEITqlghAIKRIbl8spJyXpQru2fwe1EFhNt3/xah+FuglBSeP0T5ZzVCs/S1DzqJFxtOf5pJ9aV4S5vpuSwaktU1Ra4ZCfT9oWzoK0JIfk4A49g7JH26oQXOYBG5tAbzmP5qK0nAPNy1Ke1jQTegmgnGXqFmEg0TJqiECu2c6aorLiGV9Cpcot9NGv78H4ZPwnaNOmTR3AO3XCqlWrI2e7QoJu0PkBzjuNTlBngrzEooC2OU0mxfiYw3UgkVz+tXE+ijmrthWDQ3p5GH6SHwCyIT9s70Ua8jaJXzscwvLzifbcxQYLfgK+Dr4oFNWE7YlBn24erjqVZZLp5hB0eaEtIrNsMp2af0+OlvFzxeHQgQPx4dixAeDSoGrp0KGDerzLgvaNhx/ruUXfWOfWe27saM9dcJxxpw22bsS5nlv4CvmvI39dS37bqyaP3xq0Rz6YHPqZETGuYhrTbmKTt8AVTTfu96d77qph1k16nHx/BeNWWvfTDPLq6z2XnhLmyfWFQ3p67vHrPFc+hfnyWWYBw1Lj3vizdcMO81xSQsP5dyQMHDjQ5eTkBBjvVsD70aHZ+CXBZjVz3xOgtUHt142gTw3HH3dwYx007q+3EJxcNjkHbtIrgV0I4jJSrHtyJMv5kXGrFIy78MTtD2DtYNzjf9CAMw8HcuG7xrVpZKB2JgwYMKAacFmi3Sb9ewGt+nBazuNULQAOJsdukWgRE6l1zrIQrv4tkDMzhiamvum/VdhmLgg8WE+7ywVPX+WU5G/x8eb7BsUrWQZNlP5cVaf24bjGZIPK1G4jy12/PvyE2e6U3Qr4OnpuVXnhHvTu7vD6gw43nKt7iRY92ptgYZuxxMfNdLSWaHu3AdG+c6Z26VQcV2A94aQnwyTatz60N52N9pwq3ci953uYRmelKZLJRTJKG1Npw1O1ubMbZbcCPmMB8Ok49iZbaDn06ufjtht8PHYr8PRI4J7LLU441MNbX4botTa8QGluBo+oSchU0gjMPnSS9JXUEw6xuOospiHtnD/Hwy2PGHy1QDkal+oyKQEhalq2nZbdCrim+k2PAW+MsdiwkchQLRPogXU7zsdxZzhcc6mPZx7wcfuVFi2a9Bo3RY4SzVM2vdIHr/DwKsF9/E4fPQ9lWS95uOQPwOvjfVSEdjNyOym7DXBt68pkLFrp44+jfVx/F52A0Rbvv2Kx/CsLG0uTku3QNcvhxpEOV9HZiqvvy2I1RSpI73HFBoc5dCxO/pXFvidHbtHRifqO18oiN0Cao+w2wJMJyoiLLIYONFib7+Odzx0eeM1hBO34dbcS/Ns9rJjH6rlgJXs+rj7bBXsU9Qn1OhjEQKjdmyoMnv0whN+PMihZzHJo14edEcKY2+V1Nr1b1WVS5MVGPdndJbuveCriEf0dnuCCGP6Yo0MJNU8f5v1kFs0IXd77nrAo0aNQHJwkeppBpnpESrtRt8QiWweyvYUlPl77KIRbb2U3SLjSOGMuvyiER6+2xL9pXcujeYpuV2wuorWqfrB090itVjX9FbzGpZye3zq67/ue5uPFBz0c2L1mVQJWbr9D0iE8TLd47m0beHzbF+1qGLQUo2jN0II2nLQvLXgvyOGp9x2ee5HHxQwpnEEjfTw+0qKbFutgbtTXL4P0DMbpXioZUFZbNiUlmmfXia0xbercYnsURUW7ZojV5P77Ab36Aj3YoYMIUDntrJ5CzSZwx/c1uONaH6XpwPsvAqP+5lAUuT9YV/bJBC44xuHME2inmVf3L1Po0uvW40/k5PlFLqCBB3Vx6JzNTsU5DDiQAMa74G87r9Wd/YgWR6U9AT7tUOCckxwys0hfWWZLuvkxlSbYcs6l5kefcfy50rZtW1x44YXBLbZaeymtW7dGXp5ur+waOboPgj9El8MOnHs0O0CtX5kj3gz07MgBoN39v0+BD/UYRANyRE/gvGOYni3V06ryIVvQjmfR5r8+Hvg4wrk78/xy0sMExseTNqYnAIvpCD33fnivpaYMIF+/gGXqTY2iUloVcnrtXGaQi8+YD4yZEEm4C6Rbt26YOnUq2rVrVxvw/v37o6REG7w/X6Theo2jrIzaQ3vbMg3oSC7ehuZAC9VGDsLSdQZF2rwnggkERwNR3ZiIyMq1TCGd5MzIyXPhP4BH0aaTXuPWqyt6qlXbqnpmOzGWeViO7u60SHVIY17ZZe0oSvSvSmipL75xZHKoX3oMWRc18zNbOcR51HJquG5E/xwrKxNdTtvap08fvPzyy9v+mfVvv/2WAJXtEluuUrU1q3uW2krVk6nRzmr/P/r2gp521VOwWlfrq1Zl6JnCYA+aEi1H/+jWX8A0eFHp9A5OsGHB8hWvfXCFGjutQd6gTLZP2666JgmSMK92I6PsZVcAnp6ejgMOOIDtiMH/AzPQYOXDz9xLAAAAAElFTkSuQmCC\"}}},\"1\":{\"Ident\":\"StiPageFooterBand\",\"Name\":\"PageFooterBand1\",\"ClientRectangle\":\"0,33.44,20,1\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"Components\":{\"0\":{\"Ident\":\"StiText\",\"Name\":\"Text24\",\"Guid\":\"bac617fc192f403fdd0d2b328ea8eb90\",\"ClientRectangle\":\"16.2,0.16,3.8,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{PageNofM}\"},\"HorAlignment\":\"Right\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":0,\"Bottom\":0},\"Type\":\"SystemVariables\"}}},\"2\":{\"Ident\":\"StiHeaderBand\",\"Name\":\"HeaderBand1\",\"ClientRectangle\":\"0,4.2,20,4.4\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"Components\":{\"0\":{\"Ident\":\"StiText\",\"Name\":\"Text4\",\"Guid\":\"bb59b76ca6acf408bc4e39fea79b0ee5\",\"ClientRectangle\":\"2.2,0.2,7.8,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.kepada}\"},\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\"},\"1\":{\"Ident\":\"StiText\",\"Name\":\"Text5\",\"Guid\":\"32ca6fb878630535c0ead0cc69dc705e\",\"ClientRectangle\":\"0,0.2,2.2,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Kepada : \"},\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":0,\"Top\":0,\"Bottom\":0},\"Type\":\"Expression\"},\"2\":{\"Ident\":\"StiText\",\"Name\":\"Text7\",\"Guid\":\"085f2b9a6a66a6b78f23e7fbb7c83c78\",\"ClientRectangle\":\"16.8,0.2,3.2,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.tanggal}\"},\"HorAlignment\":\"Right\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":0,\"Right\":5,\"Top\":0,\"Bottom\":0},\"TextFormat\":{\"Ident\":\"StiDateFormatService\",\"StringFormat\":\"dd/MM/yyyy\"},\"Type\":\"DataColumn\"},\"3\":{\"Ident\":\"StiText\",\"Name\":\"Text6\",\"Guid\":\"edf305e9cfa123aa98cd4f30a1756967\",\"CanShrink\":true,\"ClientRectangle\":\"0,0.8,12.4,1.2\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.alamat}\"},\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"TextOptions\":{\"WordWrap\":true},\"Margins\":{\"Left\":5,\"Right\":0,\"Top\":0,\"Bottom\":0}},\"4\":{\"Ident\":\"StiText\",\"Name\":\"Text8\",\"Guid\":\"59dfc2d54075db94f143d7b2bb59a493\",\"ClientRectangle\":\"17,0.8,3,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.username}\"},\"HorAlignment\":\"Right\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":0,\"Right\":5,\"Top\":0,\"Bottom\":0}},\"5\":{\"Ident\":\"StiText\",\"Name\":\"Text9\",\"Guid\":\"95aa703974e787de074b32b9699ab637\",\"ClientRectangle\":\"2.8,2.8,3.8,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.no_invoice}\"},\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\"},\"6\":{\"Ident\":\"StiText\",\"Name\":\"Text10\",\"Guid\":\"6083bbcbed6a2749f822d3b54c66d3d0\",\"ClientRectangle\":\"0,3.8,2.2,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"No. Trip\"},\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":0,\"Top\":0,\"Bottom\":0},\"Type\":\"Expression\"},\"7\":{\"Ident\":\"StiText\",\"Name\":\"Text11\",\"Guid\":\"f93c0a0ee8d6f6397465dbc52eea56ed\",\"ClientRectangle\":\"2.2,3.8,13.2,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Description\"},\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":0,\"Top\":0,\"Bottom\":0},\"Type\":\"Expression\"},\"8\":{\"Ident\":\"StiText\",\"Name\":\"Text12\",\"Guid\":\"9612ea9b1cb9d582ad811eba59ec6168\",\"ClientRectangle\":\"15.4,3.8,4.6,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Subtotal\"},\"HorAlignment\":\"Right\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":0,\"Bottom\":0},\"Type\":\"Expression\"},\"9\":{\"Ident\":\"StiText\",\"Name\":\"Text23\",\"Guid\":\"bde868acc320a8426687abe72c429483\",\"ClientRectangle\":\"0,2.8,2.8,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Invoice No : \"},\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":0,\"Top\":0,\"Bottom\":0},\"Type\":\"Expression\"},\"10\":{\"Ident\":\"StiHorizontalLinePrimitive\",\"Name\":\"HorizontalLinePrimitive1\",\"ClientRectangle\":\"0,2.4,20,0.0254\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"StartCap\":\";;;\",\"EndCap\":\";;;\"}}},\"3\":{\"Ident\":\"StiDataBand\",\"Name\":\"DataBand1\",\"ClientRectangle\":\"0,9.4,20,0.6\",\"Interaction\":{\"Ident\":\"StiBandInteraction\"},\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"Components\":{\"0\":{\"Ident\":\"StiText\",\"Name\":\"Text13\",\"Guid\":\"b614eb008c3b3c447fdaf2ebdd36ffc3\",\"ClientRectangle\":\"0,0,2.2,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.no_trip}\"},\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:Black\",\"Margins\":{\"Left\":5,\"Right\":0,\"Top\":0,\"Bottom\":0}},\"1\":{\"Ident\":\"StiText\",\"Name\":\"Text14\",\"Guid\":\"5dff57e3e8137e0ef907e95b9513fd0d\",\"ClientRectangle\":\"2.2,0,13.2,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.keterangan}\"},\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":0,\"Top\":0,\"Bottom\":0}},\"2\":{\"Ident\":\"StiText\",\"Name\":\"Text15\",\"Guid\":\"d99156232b3f0612c4e8fbce60fa133e\",\"ClientRectangle\":\"15.4,0,4.6,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.nilai_awal_total}\"},\"HorAlignment\":\"Right\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":0,\"Bottom\":0},\"TextFormat\":{\"Ident\":\"StiNumberFormatService\",\"NegativePattern\":1,\"GroupSeparator\":\",\",\"State\":\"DecimalSeparator\"},\"Type\":\"Expression\"}},\"DataSourceName\":\"data\"},\"4\":{\"Ident\":\"StiFooterBand\",\"Name\":\"FooterBand1\",\"ClientRectangle\":\"0,10.8,20,12\",\"BeforePrintEvent\":{\"Script\":\"if (data.getData(\\\"ppn\\\")>0) {\\r\\nText20.setEnabled(true);\\r\\nText21.setEnabled(true);\\r\\nText16.setEnabled(true);\\r\\nText26.setEnabled(true);\\r\\n} else {\\r\\nText17.setTop(0);\\r\\nText27.setTop(0);\\r\\n}\"},\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"Components\":{\"0\":{\"Ident\":\"StiText\",\"Name\":\"Text16\",\"Guid\":\"034e216f0aa91f36c273f87d41cc34ee\",\"ClientRectangle\":\"15.4,0,4.6,0.6\",\"Enabled\":false,\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.ppn}\"},\"HorAlignment\":\"Right\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":0,\"Right\":5,\"Top\":0,\"Bottom\":0},\"TextFormat\":{\"Ident\":\"StiNumberFormatService\",\"NegativePattern\":1,\"GroupSeparator\":\",\",\"State\":\"DecimalSeparator\"},\"Type\":\"DataColumn\"},\"1\":{\"Ident\":\"StiText\",\"Name\":\"Text17\",\"Guid\":\"d688b594456482bac9c6c61f4c095bcf\",\"ClientRectangle\":\"15.4,0.6,4.6,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{Sum(data.nilai_awal_total+data.ppn)}\"},\"HorAlignment\":\"Right\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":0,\"Right\":5,\"Top\":0,\"Bottom\":0},\"TextFormat\":{\"Ident\":\"StiNumberFormatService\",\"NegativePattern\":1,\"GroupSeparator\":\",\",\"State\":\"DecimalSeparator\"},\"Type\":\"Totals\"},\"2\":{\"Ident\":\"StiBarCode\",\"Name\":\"BarCode2\",\"Guid\":\"55007744d5ef627abbbc65708f9445d2\",\"ClientRectangle\":\"0,8.2,2.4,2.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Border\":\";;;;;;;solid:Black\",\"BackColor\":\"Transparent\",\"BarCodeType\":{\"Ident\":\"StiQRCodeBarCodeType\",\"ImageMultipleFactor\":0},\"Code\":{\"Value\":\"12345678901\"}},\"3\":{\"Ident\":\"StiText\",\"Name\":\"Text18\",\"Guid\":\"d1111b493282608077a0cb17205e6068\",\"ClientRectangle\":\"0,2,19.6,3.4\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Pembayaran secara Tunai dianggap tidak sah, Pembayaran secara Giro / Cheque sah apabila warkat diatas namakan {(data.ppn > 0 ? \\\"PT. SARANA RINTASINDAH\\\" : \\\"Pramudia\\\")}\\r\\nPembayaran secara transfer dinyatakan sah ke rekening dibawah ini :\\r\\n{(data.ppn > 0 ? \\\"A/n : PT. SARANA RINTASINDAH\\\" : \\\"A/n : Pramudia\\\")}\"},\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"TextOptions\":{\"WordWrap\":true},\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":5,\"Bottom\":0},\"LineSpacing\":1.5,\"Type\":\"Expression\"},\"4\":{\"Ident\":\"StiText\",\"Name\":\"Text19\",\"Guid\":\"14333ae33b500a8ae2c1cb2336503495\",\"ClientRectangle\":\"0,5.4,4.6,1.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Bank Mandiri\\r\\n{(data.ppn > 0 ? \\\"140.000.777.0242\\\" : \\\"140.00061.20225\\\")}\"},\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"TextOptions\":{\"WordWrap\":true},\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":5,\"Bottom\":0},\"LineSpacing\":1.5,\"Type\":\"Expression\"},\"5\":{\"Ident\":\"StiText\",\"Name\":\"Text20\",\"Guid\":\"37aa034660a5700958cd90315effb141\",\"ClientRectangle\":\"4.8,5.4,4.6,1.6\",\"Enabled\":false,\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Bank BRI\\r\\n032.801.001.525.302\"},\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"TextOptions\":{\"WordWrap\":true},\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":5,\"Bottom\":0},\"LineSpacing\":1.5,\"Type\":\"Expression\"},\"6\":{\"Ident\":\"StiText\",\"Name\":\"Text21\",\"Guid\":\"fcc81d9a842e95e62a54793ca2e6ce71\",\"ClientRectangle\":\"9.6,5.4,4.6,1.6\",\"Enabled\":false,\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Bank BNI\\r\\n223.355.5668\"},\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"TextOptions\":{\"WordWrap\":true},\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":5,\"Bottom\":0},\"LineSpacing\":1.5,\"Type\":\"Expression\"},\"7\":{\"Ident\":\"StiText\",\"Name\":\"Text22\",\"Guid\":\"1a53497455e1a1cffda7cacc7e8c45f0\",\"ClientRectangle\":\"0,7.2,2.6,0.8\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Authority\"},\"HorAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"TextOptions\":{\"WordWrap\":true},\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":5,\"Bottom\":0},\"LineSpacing\":1.5,\"Type\":\"Expression\"},\"8\":{\"Ident\":\"StiText\",\"Name\":\"Text26\",\"Guid\":\"3609413f76b05e6683cbb3eba2fb2477\",\"ClientRectangle\":\"11.8,0,3.6,0.6\",\"Enabled\":false,\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"PPN\"},\"HorAlignment\":\"Right\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":0,\"Bottom\":0},\"Type\":\"Expression\"},\"9\":{\"Ident\":\"StiText\",\"Name\":\"Text27\",\"Guid\":\"926936e6a4ba2f8064788ca77e6f18b1\",\"ClientRectangle\":\"11.8,0.6,3.6,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"AMOUNT DUE\"},\"HorAlignment\":\"Right\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":0,\"Bottom\":0},\"Type\":\"Expression\"}}}},\"PaperSize\":\"A4\",\"PageWidth\":21,\"PageHeight\":29.7,\"Watermark\":{\"TextBrush\":\"solid:50,0,0,0\"},\"Margins\":{\"Left\":0.5,\"Right\":0.5,\"Top\":0.5,\"Bottom\":0.5}}}}','{\"ReportVersion\":\"2020.1.1\",\"ReportGuid\":\"4f40db274696d667f4ba371eccc6427c\",\"ReportName\":\"Report\",\"ReportAlias\":\"Report\",\"ReportFile\":\"Report.mrt\",\"ReportCreated\":\"/Date(1584076314000+0700)/\",\"ReportChanged\":\"/Date(1584076314000+0700)/\",\"EngineVersion\":\"EngineV2\",\"CalculationMode\":\"Interpretation\",\"ReportUnit\":\"Centimeters\",\"PreviewSettings\":268435455,\"Dictionary\":{\"DataSources\":{\"0\":{\"Ident\":\"StiDataTableSource\",\"Name\":\"company\",\"Alias\":\"company\",\"Columns\":{\"0\":{\"Name\":\"id\",\"Index\":-1,\"NameInSource\":\"id\",\"Alias\":\"id\",\"Type\":\"System.Decimal\"},\"1\":{\"Name\":\"cabang\",\"Index\":-1,\"NameInSource\":\"cabang\",\"Alias\":\"cabang\",\"Type\":\"System.String\"},\"2\":{\"Name\":\"nama\",\"Index\":-1,\"NameInSource\":\"nama\",\"Alias\":\"nama\",\"Type\":\"System.String\"},\"3\":{\"Name\":\"alamat\",\"Index\":-1,\"NameInSource\":\"alamat\",\"Alias\":\"alamat\",\"Type\":\"System.String\"},\"4\":{\"Name\":\"kota\",\"Index\":-1,\"NameInSource\":\"kota\",\"Alias\":\"kota\",\"Type\":\"System.String\"},\"5\":{\"Name\":\"logo\",\"Index\":-1,\"NameInSource\":\"logo\",\"Alias\":\"logo\",\"Type\":\"System.String\"}},\"NameInSource\":\"source.company\"},\"1\":{\"Ident\":\"StiDataTableSource\",\"Name\":\"data\",\"Alias\":\"data\",\"Columns\":{\"0\":{\"Name\":\"id\",\"Index\":-1,\"NameInSource\":\"id\",\"Alias\":\"id\",\"Type\":\"System.Decimal\"},\"1\":{\"Name\":\"no_invoice\",\"Index\":-1,\"NameInSource\":\"no_invoice\",\"Alias\":\"no_invoice\",\"Type\":\"System.String\"},\"2\":{\"Name\":\"no_trip\",\"Index\":-1,\"NameInSource\":\"no_trip\",\"Alias\":\"no_trip\",\"Type\":\"System.String\"},\"3\":{\"Name\":\"no_pol\",\"Index\":-1,\"NameInSource\":\"no_pol\",\"Alias\":\"no_pol\",\"Type\":\"System.String\"},\"4\":{\"Name\":\"nilai_awal_total\",\"Index\":-1,\"NameInSource\":\"nilai_awal_total\",\"Alias\":\"nilai_awal_total\",\"Type\":\"System.Decimal\"},\"5\":{\"Name\":\"ppn\",\"Index\":-1,\"NameInSource\":\"ppn\",\"Alias\":\"ppn\",\"Type\":\"System.Decimal\"},\"6\":{\"Name\":\"kepada\",\"Index\":-1,\"NameInSource\":\"kepada\",\"Alias\":\"kepada\",\"Type\":\"System.String\"},\"7\":{\"Name\":\"penyewa\",\"Index\":-1,\"NameInSource\":\"penyewa\",\"Alias\":\"penyewa\",\"Type\":\"System.String\"},\"8\":{\"Name\":\"keterangan\",\"Index\":-1,\"NameInSource\":\"keterangan\",\"Alias\":\"keterangan\",\"Type\":\"System.String\"},\"9\":{\"Name\":\"alamat\",\"Index\":-1,\"NameInSource\":\"alamat\",\"Alias\":\"alamat\",\"Type\":\"System.String\"},\"10\":{\"Name\":\"tanggal\",\"Index\":-1,\"NameInSource\":\"tanggal\",\"Alias\":\"tanggal\",\"Type\":\"System.String\"},\"11\":{\"Name\":\"status\",\"Index\":-1,\"NameInSource\":\"status\",\"Alias\":\"status\",\"Type\":\"System.String\"},\"12\":{\"Name\":\"checker\",\"Index\":-1,\"NameInSource\":\"checker\",\"Alias\":\"checker\",\"Type\":\"System.String\"},\"13\":{\"Name\":\"flag\",\"Index\":-1,\"NameInSource\":\"flag\",\"Alias\":\"flag\",\"Type\":\"System.Decimal\"},\"14\":{\"Name\":\"users_id\",\"Index\":-1,\"NameInSource\":\"users_id\",\"Alias\":\"users_id\",\"Type\":\"System.Decimal\"},\"15\":{\"Name\":\"created_at\",\"Index\":-1,\"NameInSource\":\"created_at\",\"Alias\":\"created_at\",\"Type\":\"System.String\"},\"16\":{\"Name\":\"updated_at\",\"Index\":-1,\"NameInSource\":\"updated_at\",\"Alias\":\"updated_at\",\"Type\":\"System.String\"},\"17\":{\"Name\":\"deleted_at\",\"Index\":-1,\"NameInSource\":\"deleted_at\",\"Alias\":\"deleted_at\",\"Type\":\"System.String\"},\"18\":{\"Name\":\"data_invoice_header_id\",\"Index\":-1,\"NameInSource\":\"data_invoice_header_id\",\"Alias\":\"data_invoice_header_id\",\"Type\":\"System.Decimal\"},\"19\":{\"Name\":\"data_muatan_id\",\"Index\":-1,\"NameInSource\":\"data_muatan_id\",\"Alias\":\"data_muatan_id\",\"Type\":\"System.Decimal\"},\"20\":{\"Name\":\"username\",\"Index\":-1,\"NameInSource\":\"username\",\"Alias\":\"username\",\"Type\":\"System.String\"},\"21\":{\"Name\":\"password\",\"Index\":-1,\"NameInSource\":\"password\",\"Alias\":\"password\",\"Type\":\"System.String\"},\"22\":{\"Name\":\"nama\",\"Index\":-1,\"NameInSource\":\"nama\",\"Alias\":\"nama\",\"Type\":\"System.String\"}},\"NameInSource\":\"source.data\"}}},\"Pages\":{\"0\":{\"Ident\":\"StiPage\",\"Name\":\"Page1\",\"Guid\":\"1aa1713356a6ada89401e63648d10350\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Border\":\";;2;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"Components\":{\"0\":{\"Ident\":\"StiPageHeaderBand\",\"Name\":\"PageHeaderBand1\",\"ClientRectangle\":\"0,0.4,20,3\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"Components\":{\"0\":{\"Ident\":\"StiText\",\"Name\":\"Text1\",\"Guid\":\"fd9dcc0b89b91fcc9ab911fe8a8b9881\",\"ClientRectangle\":\"4.4,0.4,11.2,0.8\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"PT. SARINAH RINTAS INDAH\"},\"HorAlignment\":\"Center\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;22;Bold;\",\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:Black\",\"Type\":\"Expression\"},\"1\":{\"Ident\":\"StiText\",\"Name\":\"Text2\",\"Guid\":\"558df351a880e8975a1674bba000b032\",\"ClientRectangle\":\"3.8,1.2,12.4,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Jl. Laksda M Nasir 14, Surabaya Indonesia\"},\"HorAlignment\":\"Center\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;16;Bold;\",\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:Black\",\"Type\":\"Expression\"},\"2\":{\"Ident\":\"StiText\",\"Name\":\"Text3\",\"Guid\":\"d02a4524d1aefbd257b90ad9236db659\",\"ClientRectangle\":\"3.8,1.8,12.4,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Telp : 031-3291225 Fax : 031-3291517\"},\"HorAlignment\":\"Center\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;16;Bold;\",\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:Black\",\"Type\":\"Expression\"},\"3\":{\"Ident\":\"StiImage\",\"Name\":\"Image1\",\"Guid\":\"058e446c3a51ac0449d9a9a345d39def\",\"ClientRectangle\":\"0,0.2,2.6,2.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"ImageBytes\":\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFwAAABbCAYAAAD+6uLgAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAAJcEhZcwAADsQAAA7EAZUrDhsAACQlSURBVHhe7V0HfFbV+X7OudmDhACBEKaAgoiCCALWPVHRqnUrjrqqtT9xFG2dOKmzrrqwarX+1WqtooiislFBkFWRpWxISAgJ2fnu+T/P/b4vJIEMkJHfr756+O69Zz/nPe953nNHjKPgF9ljYiO/v8gekloaPnjIEOTlbkBKggddDEfUnQAm8huV7U2QumkkO1KO4rZXrqShuJ8j0XKj7WqsvZL603jGodIHSkp99OrdG6++8goyMjJqA56SkoqDuhbjnOMMKsod/BAv1q1nZ9oh2ZFyFMdzv04atdTwevDLcx0rTZCcx14kLsSOBumCTJFpzH+CnjIYHite4jOtxPI8uBZkokTiqyV6vabUk0ZtSk8Hlq0Gnn/PoWOPfpj4+SfIzMysDXhmmzY496g8PPwAMxXxcjkv1jU6dSuuW6mkbhpJYx2oGa84nm9tWUQi16vzRo4dfwUWlSqIig5IzWTbXAguUiKAq5/BpVqZakj0ek2pJ41jmbGdgcUzDE66yiEhsx+mTw4DXgvO2Nh4eLxCi4Ik/u7VwMYnK7AtyexYMjuRzOvBec3AaymKY/uVT3lSlD6Gv5Gg4+q80eNoqFHONm3YyaCy4lhmbBjWWlJLw9tnZWPYwHV4/k9hDS+tYIK6o7i7JdKa+MTwb1kJGx9PcxEHVPA4RDMnkyCRJsXxegzRrigCqhiXmMLrVZycZeE022jhnhC2K6kj8M03Bqfd7JCa1Q/TJm1Hw9W4AGCOjoLZC8ExqFVeGgPtoOVxLMG3rTjzIoMQTav2clLCEOTYhHBam8p8DIGdjqTbG0H4WR0HLd4qtQFvBhLDRsZwLk6bbDF1ikUcwVu+3OL9f1nkbqZG81waJMWIy+DC9JPB62Ms1uTwnAMy7kOLWdQsy4FQh5ubNDvAZfu0jjz2msGDL4c1ffIc4PejDRb8aBDbkqajkoDT9MS2Ydw8g+ufDMd51PTbnzN4/h0DJ1OzPSO6l6XZAR42Zwa5pQ7ri4WqQSl51hqe0zQTxfA0DUwfj4totwuo8uXqCc9XMl0ubX1QjtJUr1DNQ5oN4OLDAbWjmfAygc7tgG5ceJDp0IHXshIN2uucGp5Cex7HAF7nOkRsLdLb8pzXWnMB7ZRFe9+a5+zdNtRyL0uzAVwLniHoVWQbxZsNyugDhHyOQKlBSZmhM+F4jedkTqBJCQLjKgO153WlJSGX81NWwfTMw8M9z7IakWYBuOhdPDWzrMzisacsbr0fmLXUYe4PDnfcBowZ51BQ4eOJvzvcf5fByCcMbnvE4L6RBm9+QI+Y/732JvDovQbrN/uYNNdh9P0GeTlcdMlempPU5uHts3Eaefhzt4d5eNke4uECPImmIW+lRf+LgPW0wxW026o6kXa5jHY6xFbG0y7HU0Uqeaw4j/+WMZ32LJJiTBBXoEj+n8wp89UzQO9+PkrJbvaosD2JNH9iS8NGOKTUy8P3phAkmRWf7KKcIPKU7TYopskQ2JJyAl9IU1LKayUMRVUCOzAcPHfYVMF8YbxRJbbDwVLnm5PsHsAjAO2QEBgtnBkpWzPXN7vCEG8N25MOdJriZU44SM1Jmo+GEzkpo6sBUA1rt11RbHgubJuugjOhilrfjHoYyO5pTn1q15AQmxiagDjy7sZk+xDXFu0gNjdKKGlW4y+otQDuCqlSObJJzQz05gM4W6LdvkJ5iTVEeyu6OVCfCNNYzoy6aQrI5yvE2f8nTMrOSGDASU3TDNLoVXZvbzCol8XA/SxaakO8Hokn2H26WAze36JnBw/7ZHr0THmcbZGgvZRfFs1tRVuplVvIXeMd7rvG4dXbDd59BHj2FoPzDrNIia8f8MqQwcFdPDx4pYd3H3d4Y5TDSyOZ948hZLbxURHdF28m0mw0vJKuvOc5HHGUj9Mv8NE+yWLsZId/TvSRU1S/YQ+Rs0+YG8KYj3x4HLRBp/o48TQfgwY4JCaGzVRzkmYDeLC+ERzdQPh+usVF99BVf9vHV0voKcoWNyA/5fp44/MQLr7P4V9/p9GPLJZV2mepf3LsFWk2gItMxCbRg1xjMXK0wfg5PjbXWUAbErGSmcscRjwMfDmWKNO2B3d9/hdYys7y3xgumBOnAWPnRVbQBiQloCXbqu/qQh9/ewsoLjaI4wA2N2k2Gq59FN3mnrFQMNYPtijgFacbPPUXg8tPNGEmUkscpn8P5K1jmbrf2Wx6GJbd0pz69kAakuisaJXW8H5TaozBtedYXHqTwx9/65C8HZIe3LzV5WZmTiTNZvz1lJcrdTh2kEG71PqbJdYxbhLZyWaDth0MMrZjNo49hNfbMw0pYXNz75sN4AEXL6ATc6DDlcMsYutpmbZnH3mJdPEhi/J8i7OHGvTZx6ClHKYkg17tLa75DZDMgagoZYZfAN++yARUiv75Pm6mqTjlEK8el95hE9Nc+bCPl/4BDKcdf/EOOj63Gtx5tcXD1wMDDgghVBjeG/+FFjYgsr1lm4AWmSGMvonAdeUKWQ9i5SEfD70fwv3U9vg4hysuA669N4QTz/Phc+BKiyN2vJnJzwZ8l85YAkTHERU5wL4H+fjrn4CDu9XfxC3lDm9N83Hen3w8dY9B3myDmEQG2u+4ZJZF4HX7rjnJ3tdwgqyp79M2K8jb1DOEfp7DoceEMGxgJF09UkFAf1jhcM8LPoZfbvDO4xbF+eTg+wJJHZiA8cFj181EfjbgwazdWTVn5lA5fwhKvG6JtWSgax8TR5BoWkK5DsVbHJLiDY7Yl/amASkIOXyx1OHGZ3zceJfFrPEeTKpB0j4InuRqLqDvNQ3XGIXIIuTOJ3YjyB3DpiCmC5DQnb88LlxjMGkW0Jcs5NknDYafouY2ZJgdVnOAXpjg45q7+fuERWWhRQLLVK7mQBH3DuDsuKP5iKeTE98V2JxrMOM9i4lvW8wcb7BmKeHJopOTCCyix9itHdD7xBAefcDg/COl6Q2BLnH4dhU1/WmH+0YblBVQ01mGnpXb25jvGsAb638d8dnxmARqN0H44WuLu+82uOwuHxePAn57u8GIOww+I/izf7AoIkTJiYRpEdCa7OXZ0Q7XneYhoQk+ezEN+Kj3fDz5V6ZNMIhNCde9N2WPa7imtSXbiyfYS2Z6uOpOgycmOPxA3ry62Mf8HId3viHNuxO48W+G1M6hXRtmpNdYtRBIbxvCI/f6uPYMi9SEpjTf4f53fHz+MZ2pTA40J8je1PI9DrhYQ0Irgldi8MALBpOXqPs1IQifr9zsY9oPISTHGXTNYjNjHSq4wJYt1oP51Nw7wqDHiao3IoVVDne/wLwbTLAw13wUY0/LHgU80G5tLJEjL/rO4rPZ6nlDRNkhkYylBxdQ3ZvUow965aRsGV33dB+3XuswKGAvjdu0mUt9jJ9oYej+76AF3KWyxzU8+lbCEnLnjfQGGxOP7mIKBwgEPrgjxPwh2mGfi2l6Zx9HHRhO15iUc1w/nMIDcnw9kbW3GMueNykSdraqqml6VlDiME5AcdFLpC0PnCNNCg0cKWVKZAAbF4eZi31szjHwmG9veaB7B3DWqrfSmrLXUUb7+/R/gM9JG71ONCXk7Cn0Im1nYOkXHt6bqlRNU9c88n69JxS8z/e/oOECWDZYtXbp5JBBr7IpsmoTOfVjwJjHPcz+ymDRPIv3XvZw3T0OX61ouqrSgjdpkd2dssc1PLjRsAXYvy89yC5Nrd5h3hofIx4hB78PuHaUwR8ecPh0oUbPcSCbZp4yWwBtM3igN6z30sq5xwEXNpX5ooYOw0/lAtbQc2y1xKGIhverZQ5fEug1W8Ka3VSwhfCxh7C+TEdKyrM93vOw7HnAWaMe+qnKdTjjLIezDv95TdAjzY091izJamGCAYZ1Qf1NHqddLHtnnMksynPJ9FIdbrwC6JrRZKqxUyJsLzkJ6E0KWbWRg7SXtFvS9KqlRDUVqeZ5jWMpW2Mh0C6GytUO/Qc43Ha5bgbvHhTk5hx9oMUNl3H9IBkvj5oTtqPJoTFpSpqINLmX1e+RM8f2gkrSr96r8Ui7gt8GgjavQrqHSRf+sotDuPlCg6x0vSa16+a65cgO2M/D/TcYZLb1UZani8SH5n97ilBvUPqGQiRdU0RQNShiFfqAQAKdjiC0joSa5/yVU6Lj+CCY8C+vxbfhsUJwruNImlYGCdoyFfhpDtffHMJ9fzDYP3tXgK73NS1OGeTh2T85DOwbQiXBtlQE3XrTHryCjqPnekpLH1HQb/S6XmXUNU9fs4gEKUp1iJwrv5RMmt4Y8I2+NijxnAneOw1RY6IFB09KSTjCWog8an9luUEFPUPfl7qTrgVptOOngWMmHehhP+XhdcdjkZQkAh63H6+nkmN/YfHym8DYSQ5L1tDm1pqvNY/rigZJrxpadM+2OPt4h2vOBDp2Y2WlzEdHS95qICqmrqpFiw6S8CQ4j6SP/FSnkYSrC/+qrC0OpWyv7skKm/peG6wXcD1mUF7FNrYEls+yeO4fdLNZuL5jokJjeKy3fvW5I0vA4zkQRaUGhUU+QlUWwtwSUKe3g3msN9SEc2C/WaVeCVTNccyXzRkxoBdw7Mk+Wh3uULKJwH9i8dk3DrMWGixYFvYQS2WDKwxCtXrOMjjY6SkWB9D7PGqww9BBBj16+kjLYjol3WJQTA3PXWuxfl24LVq4KyvD4OgxaUeNkqmrYucq9K4n01AdkJZCPWBQu2U+9FaF+iyQta+j/Z2iMqBTho9s1hdiuiqmSaJXvMOAl7FByezE9PEWh92gFAFkDMqiX0l19u1ING1dqXvdIDvRoGe2wZW/8XHuNbyUzY4vMsghq1i90iA/12D9JmATZ55eBa+gMlRW+RxMi/atgH3Zzk50+7t29RGn4qkE4JQvXGAwforBR1MdlucYbObABd/EIlh6ikszLKoYAj9EOKo4G1WET1STaTIStA3BcwGuPAFiTBtWHioCcTpsH+CZhzjTE9i+DcSt6w4CHnwRiCOXnA0s+t7g+N9ZbChUB5mwkiaGNQXaEcPs/PWrDHQojbWRfRKPU0CNLSzmyLOxsYpj2koCRmVCQjwXMgKXUxRUTzFow/hrhlr8+S4f8W1ZYCEvs9NBl+UhVrEsqpHKC2YXLwVfD6IdDfRhI+MJgJdpMHeCxROvO4yf77COpq62CK2oKC6CXi2peW17x9EyHLq29PD9WLY52aF4JXHjAOw44OygtjIrCPDEry2WrGe/4jiKyQZxsrvUoNhYZme9PkFQQXoWxIp6CWDaTH3Fp6BAWsPB0MLD3wpO3wSCmUZvU49ErPpR78yzgf+NPhNucO9pFrffJ0RZqoDUCCnIFlM7q0Vxum2mx9qKNZistz2wdLKHS+7i7FzNCinpqUCvzpwNnOopSQ659Hb10JHWJ7BPQSc4goYaFTAPZlPfymlu4jllEtVX2lGhVUmzU0ql0RMFFYX8Zb3HH2rwyI3KTw1n2clddgJwmRRVLC319HkMzdXWTE6vLeik9rNpt6V1kJalMUgbBVqeWsbfABymUS3qu+5PkqnoOygoYdjM800W86ca3Pikj88Xau7IGhh8/bRFnzPY+xxpMq8yue5CCJBgOmsBDg4kkfhMToqfLC4ebvDBkjDYuq128eEerj7DYL/eBIODbTwf+WxnGU2WkeepRYlliCFphmhglU7uQVmhhSG4JeyryorjgJlErlMJIeRzdq5bZtC5hUN7YqMXA7RBp29e7RzgLEBKlpjOBLSTerd93RyDaTOAJWuJeRHNCxeZBGpQF3b2SDoyHQ6SZhM4zgg9warntFWOR5olQHJXA/MmWkyjfd1IwLXodkoz+Phb4Mvv/WBRlhzT3cNlZzA/Z43K073QynIbLEoBn4/VNRs2c1wJA+WgYkz5zOL+j1hhMHS8xsEdfrjFgO6c7sS1NRfpw/s6ZA5iXCumYz8C5dHskSKpPM6SEAFeO9NgFvu7cpVDPtc1kYM0sqluHSwOGuwjqx/zS8kWsQj2N/BLeLrDLCVKC4NxZ5l6zECL1ftvWPx9LDB7BTUknK1aUpm6Nxt66bEGF1zsIzWDHWQj1G/Z9URO543zLe54EviU4C4XXQsk/CslE3usbhDLk03XrI/af2mPguVqJ6ANB8to5Yvgq6+8reAg1mUyScxfIs2lxLHc/WnjTx5ocNO1DhlcaCtXcTAZrzrkHxTTVLz6MinqBOC/a3xarNrlteDMOrCLxWXHA+dd7QemtphlaMIJs/oAp03aKllZ7d3Vp3MMZhoX+gKu+BO4LR/DVc2k8vDaQ+fHuI7xnrYiVLszxjiPS7wCKwpfY2gTY9zNJ3mubIJxVdNZzliO2Vzjlv3Tc2cd4LlY0YJIGcrbJsO42FiVE75WM6g8rr2OmIZD5Bqn/tZrNYLi6pah6zFeuJ30PqvLjeO1sw723LJ3rXML4Yr+w75OZV+/Me7uMzzXKsZWl6d8yYnGJcRvLUNtaRfruYfP91xoNvv6FfFiX0vGEdYFcDNfNq5dGlz3nv3chg0bAowb1PASLkSJ8qLoGb75qsXvnqbJLZMqceFJ4HQfCvTt6VBJjVu6wuCfnzqs3RSOT+Y0HnUR7fIN1LZyLjKlFrfcZjBmOr2+IAWndW+L80926MJZsWkLZw0595gPHApKwmVI+nY1OKKfxeqcsIlTXfoKkBwtaWMg7II4cQrXmHnL6DCtre4ShVo2xOJompAErh/rSC//Q5o470cWRNFScuUxXPAe1LELHrN78xXa+2ccivTgIuWATh7OPwHo3oWziekXLaX2jwOWrQ+XkR5v8eINBr+5JIQK8nwt3KLTO6zh0m43x7iV1ID9WsmSh0c7q4V1L97huY2zrKtkfMUs4/KnW/fvhzy3T7ut6Xq0Zt73qT25cJNHW5ek+R/EGXfCQZ6b9Z7nyr9nfdR+n+Xkz7RuzMgYl5awtYzhQ6wrYNlLPzbu+3eMW/C2cXP+z7h5/F2g8+Aa3IJ/w62YZN21Q7fmVT03nGnd9+OtK6XWlnOmbmbfJr3huVP6k04wXumSOCM/GuU5lwe38TPjhnTaWkavjtZ98pzntrCv/nfhthbNse6Dp2Lc/llbyxiSbd26cUpD3D4kbvO3r+FRHdlGpPcabd3/m8aF4wexDopW8d/92uDy4Q6t0nzEVDjEcpVrmezj12QUt15gaM+oBpRl5MQz5vKANmHKLM4YLQaUjq0M7rjKof8QOillLJcMwNCet+TCeOmFdHyOZd20A5Il66l1XEu69XHo2c2h934OfXs59OFv730jobu+/EPHhw7QhhoLy5CeFndeC+bzkUBsxP5axIdfvn1wBPN0CNehj9t8MpEHVN/Z9Dlmrwz3VW248RyDE4/zOWNJ+dRW8vmUOB/DTgkFcXERBKevoVdMnAwtQvD5vnAR20i9gCtDsG9CjPQ4cLSENE7ds4dyCif4KNHKTFpVSre5hAuG6OHldM+j7+gox+LV/LeA4NP7isqBdKZ+dTgL3uSw5UfmpfdXTE5cvIQNSvdx9jFc2MS5KQvXADOm84Amp4L1VOVuGypUNineYi7Es1hGWAxOHwy07MEFcTmjmSZo5wpG0Tz1IehDOUhRWbxe7TH4kYs8LUIgooRnHsb2E2g5MyXMv4WhjG3W6n4qB6J14JRJHJb9xDL4vwjCjgNOCaw7lSAhorESlof1q3lO4GP1qdJIVIx4OCvfRADKI7ZP2bUtIRc7ia57VFay0XnLWDXz6+uassexbKR2HbVLtl6bVuEiUEEzGXxalPZbT16VU9u3CVoUGCdDUBpUGJZicfwKspkWLIPVaQ8lKEv0lPywgIMclYCKMk0a6a30TCL3Ygk5trxY7SkF28qMjNV9UfZ3DdctPe8SlQBolb+1CdtI/YAzRsZfgPfvS2AiyBZyZB99zWHVdxaxXeiBUVsTSaPi2hvkr/Iw6iUCxgVXEsPMfXswX4rDwOCBnXAZ8zn9Hn/OYDMdlLiOBvFZzM8yPPLjheTQz71HD069pXRqS/rFvD55ssDS4rhNIAiK70Ke3YPlhcXh7ckOM8d5sDRhamcy60ikyy3++S/S24+/3YrMfvuwwzSLA2i62smxo5Swr/f9w2EFF3MvI9zOBC7wXhuD1XMt/jLGIU9mhpJM29KP/oecuyptQdSDrHc3JXKMRx99DPtlb8GpR/CEmqOXnORZtWYlKwjOPE0lymJOybXU0NbsaCld+gJOxaX/NXj6JYtnPtNH7cJyZD8Pf7pMgyV7b/DdPIufmFei17SLV9lgC0S7jOs4a76YYHHv8w5TfgyniaXtv+VCi+OPpItN06VOROGsJbyoLYU4AuFVWXxKdlBJsDbSzCyk2UunepN2BlsVq9ZaTHjX4M4XHH6KKEZr2slRv6dfl8V1JIlgrjPBC7qSxRsccpZ7iK+iNtMPyc0xmPuVwV/+ZvDWt9GeWnqxHq44l+XROapkvXGcEWvXGLw5nsepWfjtZRchOZlTK1g6I7INSyEPL2Vw8+DmvhXjBvfS9lR4VdZvRpJ1R/e27uS+nuuRUZsdHNDVc589E+McV/Zy8dL5xk19yXM9AwawtYx2ZCSHd7PuELIBr/o6XCL57iUnkx1M5MpPbrzlI/Lb8fWHwF9gOrX9hnM8l1yD6aSRN5/cx3MXHWHdkT2sk+Mbjcsg43p0RCx5uHVVE+gvfA23mqxm6GD6C2Qv4XTGtfSsG9TZul8xtBR/jLRV/P6kwTFu+adkOfNM4HMU7ywPj+6l6M5GPKf23Nke/kh+On+xjxx5czXsl0S8uAVtWx+65DddwkWLC2N5gR9MMd09iSGf/3yShzufN1iy0if3Dm9qhfvOLlBTtV605fQ9mdz5tksdsjND4VtjnGmNidqaSKaSV2jx4BiL9yb5WJu71TzVlHhqfKe2FpeScd1M/hzLGVGifRXa6aS2NHuLPdxP8zhldggbCznha6wNkjiapbYZFofRjNx5NdCrZwjl5P/6goWsb5Nd+2ED1uH5P4cBj36wXQuKtl21WGzMs/j3p3TxpzgsXM6prgVLFcQbTkmDo2jvzz0J6LGPD8tB0TTWgqVa9JVMm84pu4JgTDSY9B1ZARtZVEx6yI62YdxB+xocP9DheH3vRNu3ZDDbwrV9UR3BoKVxDaVp+YxT/50vSPN+oK0t0LZuGKjW7MehfSwupFs+6NAQqWmkHuZVGdpi0Ldt8wo8fDDJYOJs4HuauZx8n/EGmVSIXly/hg4Bhh7hIy2FSkU6qj36wBmjImrzaiZp4rAba3+wvTbgWdk4c8g6PD2KJwTcF1isXD1WKm0eiVmU8WIB7dwKUiXdiRHLaM3VPTuThXNhSiXX1T627KryqyOSYCKKArCMEroARTkWm8nvi4rJ5VmGHn3TwpaaSn6vBZtUMGhdPQtQLVG66C/r00fcqzgzC/Itcsh6VuSQmLA9GbTRnanBLTvRXrcg/6fNddq4Uh2RdgYbdppRLKMs1mIL2Uwh156czSaIa0f7nEqnsQUHJY6MzLGvml0BVuqv8nfiLOGAn3id44zZ+jcgtgH8wmPX4eFHeaIFJcJSgqBUMiFqmGiVaGBUovEK0nh1QL81OhH8aidOZQp0beWqDM0/jUS0fJkY7W1rpde1mmBHy4qK4qMSjYuWI1H5CgKvZl5pgMyM6on2MVqP8keDrquvUboXFcWpnRzAYCs6iovSR+O6kv5ONfjVRVSm1v0wY0o9gB/YaR0uO5urfDlHPmIuaokKo+hPB8gTJZEI0sie6/0ZjXTQgBoN1N5HPqf0ZnJf7fppylbSJiZF7hdqKqaxYx7B199vCOpQByIgbKat37yFDIRlRvdPws3SDevwF5b1NrL27cOBsVHQlEf8mUF5ZB71AclCzsz8AhP0IZi5vC4kZIP17a3WVAh51UEZrFO2XfVL9Hx60M9IOwOTGcREhP1P5Xq1dInB/WN8tO3SF5O/HL8t4J06dkJ+7mpkpOhGbaSyeiTw0hWvX/WEITqlghAIKRIbl8spJyXpQru2fwe1EFhNt3/xah+FuglBSeP0T5ZzVCs/S1DzqJFxtOf5pJ9aV4S5vpuSwaktU1Ra4ZCfT9oWzoK0JIfk4A49g7JH26oQXOYBG5tAbzmP5qK0nAPNy1Ke1jQTegmgnGXqFmEg0TJqiECu2c6aorLiGV9Cpcot9NGv78H4ZPwnaNOmTR3AO3XCqlWrI2e7QoJu0PkBzjuNTlBngrzEooC2OU0mxfiYw3UgkVz+tXE+ijmrthWDQ3p5GH6SHwCyIT9s70Ua8jaJXzscwvLzifbcxQYLfgK+Dr4oFNWE7YlBn24erjqVZZLp5hB0eaEtIrNsMp2af0+OlvFzxeHQgQPx4dixAeDSoGrp0KGDerzLgvaNhx/ruUXfWOfWe27saM9dcJxxpw22bsS5nlv4CvmvI39dS37bqyaP3xq0Rz6YHPqZETGuYhrTbmKTt8AVTTfu96d77qph1k16nHx/BeNWWvfTDPLq6z2XnhLmyfWFQ3p67vHrPFc+hfnyWWYBw1Lj3vizdcMO81xSQsP5dyQMHDjQ5eTkBBjvVsD70aHZ+CXBZjVz3xOgtUHt142gTw3HH3dwYx007q+3EJxcNjkHbtIrgV0I4jJSrHtyJMv5kXGrFIy78MTtD2DtYNzjf9CAMw8HcuG7xrVpZKB2JgwYMKAacFmi3Sb9ewGt+nBazuNULQAOJsdukWgRE6l1zrIQrv4tkDMzhiamvum/VdhmLgg8WE+7ywVPX+WU5G/x8eb7BsUrWQZNlP5cVaf24bjGZIPK1G4jy12/PvyE2e6U3Qr4OnpuVXnhHvTu7vD6gw43nKt7iRY92ptgYZuxxMfNdLSWaHu3AdG+c6Z26VQcV2A94aQnwyTatz60N52N9pwq3ci953uYRmelKZLJRTJKG1Npw1O1ubMbZbcCPmMB8Ok49iZbaDn06ufjtht8PHYr8PRI4J7LLU441MNbX4botTa8QGluBo+oSchU0gjMPnSS9JXUEw6xuOospiHtnD/Hwy2PGHy1QDkal+oyKQEhalq2nZbdCrim+k2PAW+MsdiwkchQLRPogXU7zsdxZzhcc6mPZx7wcfuVFi2a9Bo3RY4SzVM2vdIHr/DwKsF9/E4fPQ9lWS95uOQPwOvjfVSEdjNyOym7DXBt68pkLFrp44+jfVx/F52A0Rbvv2Kx/CsLG0uTku3QNcvhxpEOV9HZiqvvy2I1RSpI73HFBoc5dCxO/pXFvidHbtHRifqO18oiN0Cao+w2wJMJyoiLLIYONFib7+Odzx0eeM1hBO34dbcS/Ns9rJjH6rlgJXs+rj7bBXsU9Qn1OhjEQKjdmyoMnv0whN+PMihZzHJo14edEcKY2+V1Nr1b1WVS5MVGPdndJbuveCriEf0dnuCCGP6Yo0MJNU8f5v1kFs0IXd77nrAo0aNQHJwkeppBpnpESrtRt8QiWweyvYUlPl77KIRbb2U3SLjSOGMuvyiER6+2xL9pXcujeYpuV2wuorWqfrB090itVjX9FbzGpZye3zq67/ue5uPFBz0c2L1mVQJWbr9D0iE8TLd47m0beHzbF+1qGLQUo2jN0II2nLQvLXgvyOGp9x2ee5HHxQwpnEEjfTw+0qKbFutgbtTXL4P0DMbpXioZUFZbNiUlmmfXia0xbercYnsURUW7ZojV5P77Ab36Aj3YoYMIUDntrJ5CzSZwx/c1uONaH6XpwPsvAqP+5lAUuT9YV/bJBC44xuHME2inmVf3L1Po0uvW40/k5PlFLqCBB3Vx6JzNTsU5DDiQAMa74G87r9Wd/YgWR6U9AT7tUOCckxwys0hfWWZLuvkxlSbYcs6l5kefcfy50rZtW1x44YXBLbZaeymtW7dGXp5ur+waOboPgj9El8MOnHs0O0CtX5kj3gz07MgBoN39v0+BD/UYRANyRE/gvGOYni3V06ryIVvQjmfR5r8+Hvg4wrk78/xy0sMExseTNqYnAIvpCD33fnivpaYMIF+/gGXqTY2iUloVcnrtXGaQi8+YD4yZEEm4C6Rbt26YOnUq2rVrVxvw/v37o6REG7w/X6Theo2jrIzaQ3vbMg3oSC7ehuZAC9VGDsLSdQZF2rwnggkERwNR3ZiIyMq1TCGd5MzIyXPhP4BH0aaTXuPWqyt6qlXbqnpmOzGWeViO7u60SHVIY17ZZe0oSvSvSmipL75xZHKoX3oMWRc18zNbOcR51HJquG5E/xwrKxNdTtvap08fvPzyy9v+mfVvv/2WAJXtEluuUrU1q3uW2krVk6nRzmr/P/r2gp521VOwWlfrq1Zl6JnCYA+aEi1H/+jWX8A0eFHp9A5OsGHB8hWvfXCFGjutQd6gTLZP2666JgmSMK92I6PsZVcAnp6ejgMOOIDtiMH/AzPQYOXDz9xLAAAAAElFTkSuQmCC\"}}},\"1\":{\"Ident\":\"StiPageFooterBand\",\"Name\":\"PageFooterBand1\",\"ClientRectangle\":\"0,33.44,20,1\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"Components\":{\"0\":{\"Ident\":\"StiText\",\"Name\":\"Text24\",\"Guid\":\"bac617fc192f403fdd0d2b328ea8eb90\",\"ClientRectangle\":\"16.2,0.16,3.8,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{PageNofM}\"},\"HorAlignment\":\"Right\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":0,\"Bottom\":0},\"Type\":\"SystemVariables\"}}},\"2\":{\"Ident\":\"StiHeaderBand\",\"Name\":\"HeaderBand1\",\"ClientRectangle\":\"0,4.2,20,4.4\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"Components\":{\"0\":{\"Ident\":\"StiText\",\"Name\":\"Text4\",\"Guid\":\"bb59b76ca6acf408bc4e39fea79b0ee5\",\"ClientRectangle\":\"2.2,0.2,7.8,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.kepada}\"},\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\"},\"1\":{\"Ident\":\"StiText\",\"Name\":\"Text5\",\"Guid\":\"32ca6fb878630535c0ead0cc69dc705e\",\"ClientRectangle\":\"0,0.2,2.2,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Kepada : \"},\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":0,\"Top\":0,\"Bottom\":0},\"Type\":\"Expression\"},\"2\":{\"Ident\":\"StiText\",\"Name\":\"Text7\",\"Guid\":\"085f2b9a6a66a6b78f23e7fbb7c83c78\",\"ClientRectangle\":\"16.8,0.2,3.2,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.tanggal}\"},\"HorAlignment\":\"Right\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":0,\"Right\":5,\"Top\":0,\"Bottom\":0},\"TextFormat\":{\"Ident\":\"StiDateFormatService\",\"StringFormat\":\"dd/MM/yyyy\"},\"Type\":\"DataColumn\"},\"3\":{\"Ident\":\"StiText\",\"Name\":\"Text6\",\"Guid\":\"edf305e9cfa123aa98cd4f30a1756967\",\"CanShrink\":true,\"ClientRectangle\":\"0,0.8,12.4,1.2\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.alamat}\"},\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"TextOptions\":{\"WordWrap\":true},\"Margins\":{\"Left\":5,\"Right\":0,\"Top\":0,\"Bottom\":0}},\"4\":{\"Ident\":\"StiText\",\"Name\":\"Text8\",\"Guid\":\"59dfc2d54075db94f143d7b2bb59a493\",\"ClientRectangle\":\"17,0.8,3,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.username}\"},\"HorAlignment\":\"Right\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":0,\"Right\":5,\"Top\":0,\"Bottom\":0}},\"5\":{\"Ident\":\"StiText\",\"Name\":\"Text9\",\"Guid\":\"95aa703974e787de074b32b9699ab637\",\"ClientRectangle\":\"2.8,2.8,3.8,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.no_invoice}\"},\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\"},\"6\":{\"Ident\":\"StiText\",\"Name\":\"Text10\",\"Guid\":\"6083bbcbed6a2749f822d3b54c66d3d0\",\"ClientRectangle\":\"0,3.8,2.2,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"No. Trip\"},\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":0,\"Top\":0,\"Bottom\":0},\"Type\":\"Expression\"},\"7\":{\"Ident\":\"StiText\",\"Name\":\"Text11\",\"Guid\":\"f93c0a0ee8d6f6397465dbc52eea56ed\",\"ClientRectangle\":\"2.2,3.8,13.2,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Description\"},\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":0,\"Top\":0,\"Bottom\":0},\"Type\":\"Expression\"},\"8\":{\"Ident\":\"StiText\",\"Name\":\"Text12\",\"Guid\":\"9612ea9b1cb9d582ad811eba59ec6168\",\"ClientRectangle\":\"15.4,3.8,4.6,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Subtotal\"},\"HorAlignment\":\"Right\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":0,\"Bottom\":0},\"Type\":\"Expression\"},\"9\":{\"Ident\":\"StiText\",\"Name\":\"Text23\",\"Guid\":\"bde868acc320a8426687abe72c429483\",\"ClientRectangle\":\"0,2.8,2.8,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Invoice No : \"},\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":0,\"Top\":0,\"Bottom\":0},\"Type\":\"Expression\"},\"10\":{\"Ident\":\"StiHorizontalLinePrimitive\",\"Name\":\"HorizontalLinePrimitive1\",\"ClientRectangle\":\"0,2.4,20,0.0254\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"StartCap\":\";;;\",\"EndCap\":\";;;\"}}},\"3\":{\"Ident\":\"StiDataBand\",\"Name\":\"DataBand1\",\"ClientRectangle\":\"0,9.4,20,0.6\",\"Interaction\":{\"Ident\":\"StiBandInteraction\"},\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"Components\":{\"0\":{\"Ident\":\"StiText\",\"Name\":\"Text13\",\"Guid\":\"b614eb008c3b3c447fdaf2ebdd36ffc3\",\"ClientRectangle\":\"0,0,2.2,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.no_trip}\"},\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:Black\",\"Margins\":{\"Left\":5,\"Right\":0,\"Top\":0,\"Bottom\":0}},\"1\":{\"Ident\":\"StiText\",\"Name\":\"Text14\",\"Guid\":\"5dff57e3e8137e0ef907e95b9513fd0d\",\"ClientRectangle\":\"2.2,0,13.2,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.keterangan}\"},\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":0,\"Top\":0,\"Bottom\":0}},\"2\":{\"Ident\":\"StiText\",\"Name\":\"Text15\",\"Guid\":\"d99156232b3f0612c4e8fbce60fa133e\",\"ClientRectangle\":\"15.4,0,4.6,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.nilai_awal_total}\"},\"HorAlignment\":\"Right\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":0,\"Bottom\":0},\"TextFormat\":{\"Ident\":\"StiNumberFormatService\",\"NegativePattern\":1,\"GroupSeparator\":\",\",\"State\":\"DecimalSeparator\"},\"Type\":\"Expression\"}},\"DataSourceName\":\"data\"},\"4\":{\"Ident\":\"StiFooterBand\",\"Name\":\"FooterBand1\",\"ClientRectangle\":\"0,10.8,20,12\",\"BeforePrintEvent\":{\"Script\":\"if (data.getData(\\\"ppn\\\")>0) {\\r\\nText20.setEnabled(true);\\r\\nText21.setEnabled(true);\\r\\nText16.setEnabled(true);\\r\\nText26.setEnabled(true);\\r\\n} else {\\r\\nText17.setTop(0);\\r\\nText27.setTop(0);\\r\\n}\"},\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Border\":\";;;;;;;solid:Black\",\"Brush\":\"solid:Transparent\",\"Components\":{\"0\":{\"Ident\":\"StiText\",\"Name\":\"Text16\",\"Guid\":\"034e216f0aa91f36c273f87d41cc34ee\",\"ClientRectangle\":\"15.4,0,4.6,0.6\",\"Enabled\":false,\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{data.ppn}\"},\"HorAlignment\":\"Right\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":0,\"Right\":5,\"Top\":0,\"Bottom\":0},\"TextFormat\":{\"Ident\":\"StiNumberFormatService\",\"NegativePattern\":1,\"GroupSeparator\":\",\",\"State\":\"DecimalSeparator\"},\"Type\":\"DataColumn\"},\"1\":{\"Ident\":\"StiText\",\"Name\":\"Text17\",\"Guid\":\"d688b594456482bac9c6c61f4c095bcf\",\"ClientRectangle\":\"15.4,0.6,4.6,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"{Sum(data.nilai_awal_total+data.ppn)}\"},\"HorAlignment\":\"Right\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":0,\"Right\":5,\"Top\":0,\"Bottom\":0},\"TextFormat\":{\"Ident\":\"StiNumberFormatService\",\"NegativePattern\":1,\"GroupSeparator\":\",\",\"State\":\"DecimalSeparator\"},\"Type\":\"Totals\"},\"2\":{\"Ident\":\"StiBarCode\",\"Name\":\"BarCode2\",\"Guid\":\"55007744d5ef627abbbc65708f9445d2\",\"ClientRectangle\":\"0,8.2,2.4,2.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Border\":\";;;;;;;solid:Black\",\"BackColor\":\"Transparent\",\"BarCodeType\":{\"Ident\":\"StiQRCodeBarCodeType\",\"ImageMultipleFactor\":0},\"Code\":{\"Value\":\"12345678901\"}},\"3\":{\"Ident\":\"StiText\",\"Name\":\"Text18\",\"Guid\":\"d1111b493282608077a0cb17205e6068\",\"ClientRectangle\":\"0,2,19.6,3.4\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Pembayaran secara Tunai dianggap tidak sah, Pembayaran secara Giro / Cheque sah apabila warkat diatas namakan {(data.ppn > 0 ? \\\"PT. SARANA RINTASINDAH\\\" : \\\"Pramudia\\\")}\\r\\nPembayaran secara transfer dinyatakan sah ke rekening dibawah ini :\\r\\n{(data.ppn > 0 ? \\\"A/n : PT. SARANA RINTASINDAH\\\" : \\\"A/n : Pramudia\\\")}\"},\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"TextOptions\":{\"WordWrap\":true},\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":5,\"Bottom\":0},\"LineSpacing\":1.5,\"Type\":\"Expression\"},\"4\":{\"Ident\":\"StiText\",\"Name\":\"Text19\",\"Guid\":\"14333ae33b500a8ae2c1cb2336503495\",\"ClientRectangle\":\"0,5.4,4.6,1.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Bank Mandiri\\r\\n{(data.ppn > 0 ? \\\"140.000.777.0242\\\" : \\\"140.00061.20225\\\")}\"},\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"TextOptions\":{\"WordWrap\":true},\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":5,\"Bottom\":0},\"LineSpacing\":1.5,\"Type\":\"Expression\"},\"5\":{\"Ident\":\"StiText\",\"Name\":\"Text20\",\"Guid\":\"37aa034660a5700958cd90315effb141\",\"ClientRectangle\":\"4.8,5.4,4.6,1.6\",\"Enabled\":false,\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Bank BRI\\r\\n032.801.001.525.302\"},\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"TextOptions\":{\"WordWrap\":true},\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":5,\"Bottom\":0},\"LineSpacing\":1.5,\"Type\":\"Expression\"},\"6\":{\"Ident\":\"StiText\",\"Name\":\"Text21\",\"Guid\":\"fcc81d9a842e95e62a54793ca2e6ce71\",\"ClientRectangle\":\"9.6,5.4,4.6,1.6\",\"Enabled\":false,\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Bank BNI\\r\\n223.355.5668\"},\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"TextOptions\":{\"WordWrap\":true},\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":5,\"Bottom\":0},\"LineSpacing\":1.5,\"Type\":\"Expression\"},\"7\":{\"Ident\":\"StiText\",\"Name\":\"Text22\",\"Guid\":\"1a53497455e1a1cffda7cacc7e8c45f0\",\"ClientRectangle\":\"0,7.2,2.6,0.8\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"Authority\"},\"HorAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\";;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"TextOptions\":{\"WordWrap\":true},\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":5,\"Bottom\":0},\"LineSpacing\":1.5,\"Type\":\"Expression\"},\"8\":{\"Ident\":\"StiText\",\"Name\":\"Text26\",\"Guid\":\"3609413f76b05e6683cbb3eba2fb2477\",\"ClientRectangle\":\"11.8,0,3.6,0.6\",\"Enabled\":false,\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"PPN\"},\"HorAlignment\":\"Right\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":0,\"Bottom\":0},\"Type\":\"Expression\"},\"9\":{\"Ident\":\"StiText\",\"Name\":\"Text27\",\"Guid\":\"926936e6a4ba2f8064788ca77e6f18b1\",\"ClientRectangle\":\"11.8,0.6,3.6,0.6\",\"Interaction\":{\"Ident\":\"StiInteraction\"},\"Text\":{\"Value\":\"AMOUNT DUE\"},\"HorAlignment\":\"Right\",\"VertAlignment\":\"Center\",\"Font\":\"Times New Roman;14;;\",\"Border\":\"All;;;;;;;solid:0,0,0\",\"Brush\":\"solid:Transparent\",\"TextBrush\":\"solid:0,0,0\",\"Margins\":{\"Left\":5,\"Right\":5,\"Top\":0,\"Bottom\":0},\"Type\":\"Expression\"}}}},\"PaperSize\":\"A4\",\"PageWidth\":21,\"PageHeight\":29.7,\"Watermark\":{\"TextBrush\":\"solid:50,0,0,0\"},\"Margins\":{\"Left\":0.5,\"Right\":0.5,\"Top\":0.5,\"Bottom\":0.5}}}}','2020-03-05 16:22:33','2020-03-06 09:17:14');
/*!40000 ALTER TABLE `print_out` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_role` varchar(200) NOT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_nama_role_uindex` (`nama_role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'ADMINISTRATOR','ADMINISTRATOR','2020-03-07 13:31:45','2020-03-07 13:31:45',NULL),(2,'JANCOK','JANCOK','2020-03-07 07:12:44','2020-03-07 07:12:56','2020-03-07 07:12:56'),(3,'ONTOL','KNC','2020-03-07 07:13:05','2020-03-07 07:13:15','2020-03-07 07:13:15');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_modul`
--

DROP TABLE IF EXISTS `roles_modul`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles_modul` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roles_id` int(11) NOT NULL,
  `modul_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `roles_modul_modul_id_fk` (`modul_id`),
  KEY `roles_modul_roles_id_fk` (`roles_id`),
  CONSTRAINT `roles_modul_modul_id_fk` FOREIGN KEY (`modul_id`) REFERENCES `modul` (`id`),
  CONSTRAINT `roles_modul_roles_id_fk` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_modul`
--

LOCK TABLES `roles_modul` WRITE;
/*!40000 ALTER TABLE `roles_modul` DISABLE KEYS */;
INSERT INTO `roles_modul` VALUES (16,1,1,NULL,0),(17,1,27,2,0),(18,1,29,2,0),(19,1,22,30,0),(20,1,30,2,0),(21,1,2,NULL,0),(22,1,7,3,0),(23,1,9,3,0),(24,1,11,3,0),(25,1,13,3,0),(26,1,3,NULL,0),(27,1,15,4,0),(28,1,18,4,0),(29,1,20,4,0),(30,1,4,NULL,0),(31,1,5,NULL,0);
/*!40000 ALTER TABLE `roles_modul` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_modul_access`
--

DROP TABLE IF EXISTS `roles_modul_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles_modul_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roles_id` int(11) NOT NULL DEFAULT '0',
  `modul_access_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `roles_modul_access_modul_access_id_fk` (`modul_access_id`),
  KEY `roles_modul_access_roles_id_fk` (`roles_id`),
  CONSTRAINT `roles_modul_access_modul_access_id_fk` FOREIGN KEY (`modul_access_id`) REFERENCES `modul_access` (`id`),
  CONSTRAINT `roles_modul_access_roles_id_fk` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_modul_access`
--

LOCK TABLES `roles_modul_access` WRITE;
/*!40000 ALTER TABLE `roles_modul_access` DISABLE KEYS */;
INSERT INTO `roles_modul_access` VALUES (1,1,1);
/*!40000 ALTER TABLE `roles_modul_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(2000) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `roles_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `users_roles_id_fk` (`roles_id`),
  CONSTRAINT `users_roles_id_fk` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','$2y$10$lXeRrmPnr0VOdFKBkuHVF.XE6kUBcpweTcU0kdrHOQQHtF66dFECS','ADMINISTRATOR',1,'2020-03-01 03:29:04','2020-03-01 03:29:04',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-07 23:44:49
