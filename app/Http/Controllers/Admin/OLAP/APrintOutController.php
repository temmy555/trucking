<?php

namespace App\Http\Controllers\Admin\OLAP;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\PrintOut;
use DB;
use Illuminate\Http\Request;
use Vendor\Package\Stimulsoft\Classes\StiResult as CoreResult;
use Vendor\Package\Stimulsoft\CoreHandler;
use Vendor\Package\Stimulsoft\CoreHelper;

class APrintOutController extends Controller
{
    public function index(Request $request)
    {
//        PrintOut::updateOrCreate(
//            ['nama' => $request->input('print')],
//            ['design' => '{}']
//        );
//        return '';
        if ($request->isMethod('post')) {
            return $this->handle($request);
        }
        $data = $this->getData($request);
        if (!$data) {
            return Helper::redirect('home', Constant::AlertWarning, Constant::TitleWarning, 'no data found');
        }
        $print_out = PrintOut::firstOrCreate(['nama' => $request->input('print')]);
        $designer = ($print_out->design) ? $print_out->design : $print_out->design_def;
        $options = CoreHelper::createOptions();
        $options->handler = $request->fullUrl();
        $options->timeout = 30;
        $core = new CoreHelper();
        $core->setOptions($options);
        $is_design = $request->has('hm');
        return view('admin.menus.olap.print_out', compact('is_design', 'core', 'designer', 'data'));
    }

    public function getData(Request $request)
    {
        $printOut = $request->input('print');
        $uid = $request->input('id');
        $data = [];
        switch ($printOut) {
            case Constant::PrintInvoice:
                $data = DB::select(DB::raw("select *
                    from data_invoice_print p
                    join data_invoice_header h on p.data_invoice_header_id = h.id
                    join users u on p.users_id = u.id
                    where p.data_invoice_header_id = '" . $uid . "'"));
                break;
            default:
                break;
        }
        if (count($data) < 1) {
            return false;
        }
        $result = [
            'company' => Company::first(),
            'data' => $data,
        ];
        return $result;
    }

    public function handle(Request $request)
    {
        $handler = new CoreHandler();
        $handler->registerErrorHandlers();

        $handler->onBeginProcessData = function ($event) {
            $database = $event->database;
            $connection = $event->connection;
            $dataSource = $event->dataSource;
            $connectionString = $event->connectionString;
            $queryString = $event->queryString;
            return CoreResult::success();
        };

        $handler->onPrintReport = function ($event) {
            return CoreResult::success();
        };

        $handler->onBeginExportReport = function ($event) {
            $settings = $event->settings;
            $format = $event->format;
            return CoreResult::success();
        };

        $handler->onEndExportReport = function ($event) {
            $format = $event->format;
            $data = $event->data;
            $fileName = $event->fileName;
            file_put_contents('reports/' . $fileName . '.' . strtolower($format), base64_decode($data));
            return CoreResult::success("Export OK. Message from server side.");
        };
        $handler->onEmailReport = function ($event) {
            $event->settings->from = "******@gmail.com";
            $event->settings->host = "smtp.gmail.com";
            $event->settings->login = "******";
            $event->settings->password = "******";
        };

        $handler->onDesignReport = function ($event) {
            return CoreResult::success();
        };

        $handler->onCreateReport = function ($event) {
            $fileName = $event->fileName;
            return CoreResult::success();
        };

        $handler->onSaveReport = function ($event) use ($request) {
            $report = $event->report;
            $reportJson = $event->reportJson;
            $fileName = $event->fileName;

            PrintOut::updateOrCreate(
                ['nama' => $request->input('print')],
                ['design' => $reportJson]
            );
//            file_put_contents('reports/' . $fileName . ".mrt", $reportJson);
            return CoreResult::success("Save Report OK: " . $fileName);
        };

        $handler->onSaveAsReport = function ($event) {
            return CoreResult::success();
        };

        return $handler->process();
    }
}
