<?php

namespace App\Http\Controllers\Admin\Sistem;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Validator;
use Yajra\DataTables\DataTables;

class ASistemUserController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        $role = Role::get();
        if ($uid) {
            $detail = User::with(['role'])->find($uid);
            $role = Role::get();
//            return $detail;
            return view('admin.menus.sistem.user.detail', compact('detail','role'));
        }
        return view('admin.menus.sistem.user.detail',compact('role'));
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.sistem.user.list');
    }
    public function ganti_password(Request $request)
    {
        $user_id = auth()->id();
        $user = DB::table('users')
            ->select('username')
            ->where('id',$user_id)
            ->get();

//            User::select('username')->where('id',$user_id)->get();
//        return $user;
        return view('admin.menus.sistem.user.ganti_password',compact('user'));
    }
    public function updatePassword(Request $request)
    {
        $username=$request->input('username');
        $password=$request->input('currentpassword');
        if (Auth::attempt(['username' => $username, 'password' => $password])) {
            if($request->input('newpassword') == $request->input('confnewpassword')){
                $user = User::find(Auth::id());
                $user->password = Hash::make($request->input('newpassword'));
                $user->save();
                Auth::logout();
                return redirect()->route('login');
            }else{
                $message="_______Password Baru tidak sama dengan Konfirmasi Password Baru";
                return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $message);
            }
        }else{
            $message="_______Password Lama Tidak Cocok";
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $message);
        }
    }

    public function postDetail(Request $request)
    {
//        return $request->all();
        $uid = $request->input('id');
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'username' => 'required|min:3|max:100',
            'nama' => 'required|min:3|max:100',
            'password' => 'nullable|min:5|required_with:validasi_password|same:validasi_password',
            'validasi_password' => 'nullable|min:5',
        ], $message);

        $inputs = Helper::merge($request);

        try {
            $uid = $inputs->input('id');
            $message = 'User Berhasil Dibuat';

            $data = new User();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = User::find($uid);
                $message = 'User Berhasil Diedit';
            }
            $username = $inputs->input('username');
            $nama = $inputs->input('nama');
            $roles_id = $inputs->input('roles_id');
            $password = $inputs->input('password');
            if ($password) {
                $data->password = Hash::make($password);
            }
            $data->username = $username;
            $data->nama = $nama;
            $data->roles_id = $roles_id;
            $data->save();

            return Helper::redirect('user.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }

    }

    public function deleteData(Request $request)
    {
        $message = 'User Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }
        User::find($uid)->delete();

        return Helper::redirect('user.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
//        $field = ['user_id', 'username', 'nama', 'role_id'];
//        $model = DB::table('beone_user')->select($field)->whereNull('deleted_at');
        $model = User::query()->with(['role']);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($data) {
                return
                    '<a href="' . route('user.detail', ['id' => $data->id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . route('user.delete', ['id' => $data->id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function IsInvalid($uid)
    {
        return false;
    }
}
