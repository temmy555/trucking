<?php

namespace App\Http\Controllers\Admin\report;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\DataMuatan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class AReportController extends Controller
{
    public function index(Request $request)
    {
        $timezone = 'ASIA/JAKARTA';
        $today = Carbon::today()->setTimezone($timezone)->toDateString();
        $data = DataMuatan::with(['users'])->where('tanggal',$today)->get();
        return view('admin.menus.report.list_today',['data' => $data]);
    }
    public function index2(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable2($request);
        }
        return view('admin.menus.report.list_all');
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function dataTable(Request $request)
    {
        $timezone = 'ASIA/JAKARTA';
        $today = Carbon::today()->setTimezone($timezone)->toDateString();
        $model = DataMuatan::query()->with(['users'])->where('tanggal',$today);
        $datatable = Datatables::of($model)
            ->editColumn('tanggal', function ($data){
                return date('Y-m-d', strtotime($data->tanggal) );
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }
    public function dataTable2(Request $request)
    {
        $model = DataMuatan::query()->with(['users']);
        $datatable = Datatables::of($model)
            ->editColumn('tanggal', function ($data){
                return date('Y-m-d', strtotime($data->tanggal) );
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }
}
