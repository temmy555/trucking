<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $nama
 * @property int $roles_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Role $role
 * @property Collection|DataInvoiceHeader[] $data_invoice_headers
 * @property Collection|DataInvoicePrint[] $data_invoice_prints
 * @property Collection|DataMuatan[] $data_muatans
 * @property Collection|Pelunasan[] $pelunasans
 *
 * @package App\Models
 */
class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
	protected $table = 'users';

	protected $casts = [
		'roles_id' => 'int'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'username',
		'password',
		'nama',
		'roles_id'
	];

	public function role()
	{
		return $this->belongsTo(Role::class, 'roles_id');
	}

	public function data_invoice_headers()
	{
		return $this->hasMany(DataInvoiceHeader::class, 'users_id');
	}

	public function data_invoice_prints()
	{
		return $this->hasMany(DataInvoicePrint::class, 'users_id');
	}

	public function data_muatans()
	{
		return $this->hasMany(DataMuatan::class, 'users_id');
	}

	public function pelunasans()
	{
		return $this->hasMany(Pelunasan::class, 'users_id');
	}
}
