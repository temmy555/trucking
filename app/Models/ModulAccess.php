<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ModulAccess
 * 
 * @property int $id
 * @property string $access
 * @property int $modul_id
 * 
 * @property Modul $modul
 *
 * @package App\Models
 */
class ModulAccess extends Model
{
	protected $table = 'modul_access';
	public $timestamps = false;

	protected $casts = [
		'modul_id' => 'int'
	];

	protected $fillable = [
		'access',
		'modul_id'
	];

	public function modul()
	{
		return $this->belongsTo(Modul::class);
	}
}
