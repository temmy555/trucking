<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Report
 *
 * @property int $id
 * @property string $code
 * @property string $type
 * @property string $name
 * @property string $query
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class Report extends Model
{
    use SoftDeletes;
    protected $table = 'report';

    protected $fillable = [
        'code',
        'type',
        'name',
        'query'
    ];

    public $appends = ['modul_id', 'parent_id', 'text', 'spriteCssClass'];

    function getSpriteCssClassAttribute()
    {
        return null;
    }

    function getModulIdAttribute()
    {
        return "R" . $this->id;
    }

    function getParentIdAttribute()
    {
        return 5; //paten sesuai laporan
    }

    function getTextAttribute()
    {
        return $this->code . " (" . $this->name . ")";
    }
}
