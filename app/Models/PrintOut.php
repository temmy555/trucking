<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PrintOut
 * 
 * @property int $id
 * @property string $nama
 * @property string $design
 * @property string $design_def
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class PrintOut extends Model
{
	protected $table = 'print_out';

	protected $fillable = [
		'nama',
		'design',
		'design_def'
	];
}
