<?php

namespace Vendor\Package\Stimulsoft;

use Illuminate\Support\ServiceProvider;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->alias('core.helper', CoreHelper::class);
        $this->app->singleton('core.helper', function () {
            return new CoreHelper;
        });

        $this->app->alias('core.handler', CoreHandler::class);
        $this->app->singleton('core.handler', function () {
            return new CoreHandler;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
