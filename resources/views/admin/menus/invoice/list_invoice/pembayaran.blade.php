@extends('admin.layouts.default')
@section('title', $title='Pembayaran')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs" method="post"
                          action="{{route('kwitansi.pelunasan')}}">
                        @csrf
                        @foreach ($no_kwitansi as $nk)
                        <input type="hidden" class="form-control" name="header_id"
                               value="{{$nk->id}}">
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>No. Invoice:</label>
                                        <input type="text" name="no_kwitansi" class="form-control"
                                               placeholder="Masukkan No. Invoice" value="{{$nk->no_invoice}}"
                                               readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Tanggal:</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control date-picker"
                                                   name="tanggal_bayar"
                                                   placeholder="Select date"/>
                                            <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="la la-calendar-check-o"></i>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Jumlah Tagihan :</label>
                                        <input type="text"
                                               name="jumlah_tagihan" value="{{$nk->total_tagihan}}"
                                               class="form-control price input-currency" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Nominal Dibayar :</label>
                                        <input type="text"
                                               name="nominal_dibayar"
                                               class="form-control price input-currency" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Penyewa:</label>
                                        <input type="text" name="penyewa" class="form-control" value="{{$nk->penyewa}}" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Bank</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2"
                                                    name="bank"
                                                    required>
                                                <option></option>
                                                <option>BCA SRI</option>
                                                <option>BCA PRAMUDIA</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2"
                                                    name="status"
                                                    required>
                                                <option></option>
                                                <option>NONE</option>
                                                <option>PARTIAL</option>
                                                <option>PAID</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="row">
                                        <div class="col-lg-12 ml-lg-auto">
                                            <button type="submit" class="btn btn-brand">Submit</button>
                                            <a data-url="{{url()->previous()}}"
                                               class="btn btn-secondary prevent-dialog"
                                               data-sw-title="Yakin Cancel?">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </form>
                </div>
            </div>
        </div>
    </div>
     <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                               @foreach ($no_kwitansi as $nk2)
                                    <h2><strong>Invoice #{{$nk2->no_invoice}}</strong></h2>
                               @endforeach
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <table class="table table-striped- table-bordered table-hover" id="table-dt">
                            <thead>
                            <tr>
                                <th>Jumlah Tagihan</th>
                                <th>Tanggal Bayar</th>
                                <th>Penyewa</th>
                                <th>Nominal Dibayar</th>
                                <th>Bank</th>
                                <th>Status</th>
                                <th>Audit</th>
                                <th>Checker</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $d)
                                <tr>
                                    <td>
                                        Rp {{ number_format($d->jumlah_tagihan,'0', '.', ',') }}</td>
                                    <td>{{ date('d-m-Y',strtotime($d->tanggal_bayar)) }}</td>
                                    <td>{{$d->penyewa}}</td>
                                    <td>
                                        Rp {{ number_format($d->nominal_dibayar,'0', '.', ',') }}</td>
                                    <td>{{$d->bank}}</td>
                                    <td>{{$d->status}}</td>
                                    <td>
                                        @if($d->audit == 0)
                                            <a href="{{url('invoice/kwitansi/audit?id='.$d->id)}}"
                                               class="btn btn-sm btn-danger">NO</a>
                                        @else
                                            <a href="{{url('invoice/kwitansi/audit?id='.$d->id)}}"
                                               class="btn btn-sm btn-success">YES</a>
                                        @endif
                                    </td>
                                    <td>{{$d->users->username}}</td>
                                    <td>
                                        @if($d->audit == 0)
                                            <a href="{{url('order/delete?id=')}}"
                                               class="btn btn-sm btn-danger"><i
                                                    class="la la-trash"></i></a>
                                        @else
                                            <span
                                                class="kt-badge kt-badge--success kt-badge--inline">SUDAH AUDIT</span>
                                        @endif
                                    </td>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
     </div>
@endsection
@push('script')
@endpush
