@extends('admin.layouts.default')
@section('title', $title='Input Kwitansi')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs" method="post"
                          action="{{route('kwitansi.detail')}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @foreach($detail as $d)
                                <input type="hidden" class="form-control" name="data_muatan_id[]"
                                       value="{{$d->id}}">
                            @endforeach
                            <input type="hidden" class="form-control" name="penyewa"
                                   value="{{$penyewa}}">
                            <div class="form-group">
                                <label>No. Invoice:</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();" name="no_invoice" class="form-control"
                                       placeholder="Masukkan No. Invoice"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>No. Trip:</label>
                                <input type="text" name="no_trip" class="form-control"
                                       value="{{$no_trip}}"
                                       readonly>
                            </div>
                            <div class="form-group">
                                <label>Tanggal:</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control date-picker"
                                           name="tanggal"
                                           placeholder="Select date"/>
                                    <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden"
                                   name="nilai_awal_total"
                                   value="{{$nilai_awal_total}}"
                                   class="form-control price input-currency" readonly>
                            <div class="form-group">
                                <label>Jumlah Tagihan:</label>
                                <input type="text"
                                       name="total_tagihan"
                                       class="form-control price input-currency" required>
                            </div>
                            <div class="form-group">
                                <label>Pajak:</label>
                                <input type="text"
                                       name="ppn"
                                       class="form-control price input-currency" required>
                            </div>
                            <div class="form-group">
                                <label>Keterangan:</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();" name="keterangan" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Kepada:</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();" name="kepada" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Alamat:</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();" name="alamat" class="form-control">
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="row">
                                        <div class="col-lg-12 ml-lg-auto">
                                            <button type="submit" class="btn btn-brand">Submit</button>
                                            <a data-url="{{url()->previous()}}"
                                               class="btn btn-secondary prevent-dialog"
                                               data-sw-title="Yakin Cancel?">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
