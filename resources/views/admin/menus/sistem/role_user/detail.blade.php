@extends('admin.layouts.default')
@section('title', $title='Sistem Role User')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form id="form" class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->id}}"/>
                            @endisset
                            <div class="form-group">
                                <label>Nama Role User:</label>
                                <input type="text" name="nama_role" class="form-control" placeholder="Nama"
                                       value="{{old('nama_role',(isset($detail)? $detail->nama_role : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Keterangan:</label>
                                <input type="text" name="keterangan" class="form-control" placeholder="Keterangan"
                                       value="{{old('keterangan',(isset($detail)? $detail->keterangan : ''))}}">
                            </div>
                            <div class="kt-separator kt-separator--border-dashed"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="d-flex bd-highlight">
                                        <div class="p-2 flex-grow-1 bd-highlight">All Moduls:</div>
                                        <div class="p-2 bd-highlight">
                                            <button type="button" id="add_all" class="btn btn-primary">Add All</button>
                                        </div>
                                    </div>
                                    <div class="box p-3 border border-info">
                                        <div id="all_modul" style="min-height: 700px"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex bd-highlight">
                                        <div class="p-2 flex-grow-1 bd-highlight">Active Moduls:</div>
                                        <div class="p-2 bd-highlight">
                                            <button type="button" id="remove_all" class="btn btn-primary">Remove All
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box p-3 border border-success">
                                        <div id="act_modul" style="min-height: 500px"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--border-dashed"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Modul Access:</p>
                                    <div id="gridContainer"></div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
    <style>
        .fa.k-sprite,
        .fa.k-sprite::before {
            padding-top: 2px;
            margin-top: 2px;
            font-size: 12px;
            line-height: 12px;
            vertical-align: middle;
        }

        .k-group:empty::after {
            content: 'Drop Menu Here';
            color: #333333;
            font-size: 20px;
        }

    </style>
@endpush
@push('script')
    <script>
        $(function () {
            thisForm.init();
        }), thisForm = {
            init: function () {
                thisForm.treeView();
                thisForm.dataGrid();
                $('#form').submit(function (e) {
                    // e.preventDefault();
                    // console.log($access.selectedKeyNames());
                    // return false;
                    $data = $act_modul.dataSource.view();
                    $data = JSON.stringify($data);

                    $data_access = $access.selectedKeyNames();
                    $data_access = JSON.stringify($data_access);

                    $('<input>', {
                        type: 'hidden',
                        name: 'moduls_access',
                        value: $data_access
                    }).appendTo($(this));

                    $('<input>', {
                        type: 'hidden',
                        name: 'moduls',
                        value: $data
                    }).appendTo($(this));
                    return true;
                });
            },
            treeView: function () {
                $all_modul = $("#all_modul").kendoTreeView({
                    dragAndDrop: true,
                    dataSource: @json($all_modul),
                }).data("kendoTreeView");

                $act_modul = $("#act_modul").kendoTreeView({
                    dragAndDrop: true,
                    dataSource: @json($act_modul),
                }).data("kendoTreeView");

                $all_modul.expand(".k-item");
                $act_modul.expand(".k-item");

                $('#remove_all').on('click', function () {
                    if (!$act_modul.dataSource.view().length > 0) return;
                    $all_modul.append($act_modul.dataSource.view());
                    $act_modul.dataSource.data([]);
                });
                $('#add_all').on('click', function () {
                    if (!$all_modul.dataSource.view().length > 0) return;
                    $act_modul.append($all_modul.dataSource.view());
                    $all_modul.dataSource.data([]);
                });
            },
            dataGrid: function () {
                $access = $("#gridContainer").kendoGrid({
                    dataSource: {
                        transport: {
                            read: {
                                data: {id: '{{(isset($detail)? $detail->id : 0)}}'},
                                url: "{{url()->current()}}",
                                dataType: "jsonp",
                            }
                        },
                        schema: {
                            model: {
                                id: "id"
                            }
                        },
                        group: {field: "modul"}
                    },
                    selectable: "multiple",
                    filterable: true,
                    columnMenu: true,
                    sortable: true,
                    resizable: true,
                    reorderable: true,
                    groupable: true,
                    persistSelection: true,
                    dataBound: onDataBound,
                    pageable: false,
                    columns: [
                        {selectable: true, width: "50px"},
                        {field: "modul", title: "Modul"},
                        {field: "access", title: "Akses"},
                    ]
                }).data("kendoGrid");

            }
        };

        function onDataBound(e) {
            const grid = this;
            const rows = grid.items();

            $(rows).each(function (e) {
                const row = this;
                const dataItem = grid.dataItem(row);
                if (dataItem.granted > 0) {
                    grid.select(row);
                }
            });
            //EDIT EXAMPLE
            {{--$("#gridContainer .edit-data").on('click', function () {--}}
            {{--    const dataItem = grid.dataItem($(this).closest('tr'));--}}
            {{--    $link = "{{route('report.all')}}" + "?id=" + dataItem.id;--}}
            {{--    // location.href = $link; //ini langsung redirect--}}
            {{--    swal.fire({--}}
            {{--        title: $title,--}}
            {{--        type: 'warning',--}}
            {{--        showCancelButton: true,--}}
            {{--        confirmButtonText: 'Ya',--}}
            {{--        cancelButtonText: 'Tidak'--}}
            {{--    }).then(function (result) {--}}
            {{--        if (result.value) {--}}
            {{--            location.href = $link;--}}
            {{--        }--}}
            {{--    });--}}
            {{--});--}}
        }
    </script>
@endpush
