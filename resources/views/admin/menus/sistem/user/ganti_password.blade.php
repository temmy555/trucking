@extends('admin.layouts.default')
@section('title', $title='Ganti Password')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{route('password.update')}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            <div class="form-group">
                                <label>Username:</label>
                                @foreach($user as $u)
                                    <input type="text" name="username" class="form-control" placeholder="Username"
                                           value="{{$u->username}}"
                                           readonly>
                                @endforeach
                            </div>
                            <div class="form-group">
                                <label>Current Password:</label>
                                <input type="password" name="currentpassword" class="form-control"
                                       placeholder="Masukkan Password Lama">
                            </div>
                            <div class="form-group">
                                <label>Password Baru:</label>
                                <input type="password" name="newpassword" class="form-control"
                                       placeholder="Masukkan Password Baru">
                            </div>
                            <div class="form-group">
                                <label>Konfirmasi Password:</label>
                                <input type="password" name="confnewpassword"
                                       class="form-control"
                                       placeholder="Masukkan Konfirmasi Password">
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('#password, #validasi_password').on('keyup', function () {
            if ($('#password').val() == $('#validasi_password').val()) {
                $('#message').html('Password Matching').css('color', 'green');
            } else
                $('#message').html('Password Not Matching').css('color', 'red');
        });
    </script>
@endpush
