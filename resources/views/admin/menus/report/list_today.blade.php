@extends('admin.layouts.default')
@section('title', $title='Data Today')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        @include('admin.includes.alert')
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <table class="table table-striped- table-bordered table-hover" id="table-dt">
                            <thead>
                            <tr>
                                <th>Penerima</th>
                                <th>Jenis Biaya</th>
                                <th>Tanggal</th>
                                <th class="currency">Jumlah</th>
                                <th>Pengirim</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $d)
                                <tr>
                                    <td>{{$d->penerima}}</td>
                                    <td>{{$d->jenis_biaya}}</td>
                                    <td>{{ date('Y-m-d',strtotime($d->tanggal)) }}</td>
                                    <td>{{ number_format($d->jumlah,'0', '.', ',') }}</td>
                                    <td>{{$d->users->username}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="3" style="text-align:right">GRAND TOTAL =</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('#table-dt').DataTable({
            lengthMenu: [
                [-1],
                ['Show all']
            ],
            "searching": false,
            "paging": false,
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(),
                    data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    i = String(i).replace('Rp', '')
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column(3)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over this page
                pageTotal = api
                    .column(3, {
                        page: 'current'
                    })
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                // console.log(pageTotal);
                // Update footer
                // console.log($(api.column(3).footer().html));
                $(api.column(3).footer()).html(
                    pageTotal.toLocaleString((undefined, {
                        minimumFractionDigits: 0,
                        maximumFractionDIgit: 0
                    }))
                );
            }
        });
    </script>
@endpush
