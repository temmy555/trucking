@extends('admin.layouts.default')
@section('title', $title='OLAP')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs" method="post" target="_blank"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->user_id}}"/>
                            @endisset
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Tipe Query:</label>
                                        <div class="kt-radio-inline">
                                            <label class="kt-radio">
                                                <input type="radio" name="tipe" value="pgsql"
                                                    {{(old('tipe',(isset($detail)? $detail->tipe : 'pgsql'))=='pgsql'? 'checked':'')}}
                                                    {{(isset($detail)? 'disabled="disabled"' : '')}}
                                                > Postgresql
                                                <span></span>
                                            </label>
                                            <label class="kt-radio">
                                                <input type="radio" name="tipe" value="mysql"
                                                    {{(old('tipe',(isset($detail)? $detail->tipe : ''))=='mysql'? 'checked':'')}}
                                                    {{(isset($detail)? 'disabled="disabled"' : '')}}
                                                > Mysql
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Query :</label>
                                        <textarea name="query" class="form-control" data-provide="markdown"
                                                  rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Execute</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')

@endpush
