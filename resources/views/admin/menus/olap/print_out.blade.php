<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="{{ asset('css/print_out.css') }}" rel="stylesheet" type="text/css"/>
    <script src="{{ asset('js/print_out.js') }}"></script>
    @if($is_design)
        {!! $core->designer($designer, 'content', json_encode($data)) !!}
    @else
        {!! $core->viewer($designer, 'content', json_encode($data)) !!}
    @endif
</head>
<body>
<div id="content"></div>
</body>
</html>
