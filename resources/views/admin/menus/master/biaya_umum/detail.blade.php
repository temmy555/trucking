@extends('admin.layouts.default')
@section('title', $title='Biaya Umum')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->id}}"/>
                            @endisset
                            <div class="form-group">
                                <label>Penerima:</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();" name="penerima"
                                       class="form-control" placeholder="Penerima"
                                       value="{{old('penerima',(isset($detail)? $detail->penerima : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Jenis Biaya</label>
                                <div class="input-group">
                                    <select class="form-control kt-select2"
                                            name="jenis_biaya"
                                            required>
                                        <option value="SPARE PART">SPARE PART</option>
                                        <option value="BAN">BAN</option>
                                        <option value="UANG MAKAN">UANG MAKAN</option>
                                        <option value="UANG GAJI">UANG GAJI</option>
                                        <option value="STORING">STORING</option>
                                        <option value="LAIN-LAIN">LAIN - LAIN</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Tanggal:</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control date-picker"
                                           name="tanggal"
                                           value="{{old('tanggal',(isset($detail)? $detail->tanggal : ''))}}"
                                           placeholder="Select date"
                                        {{(isset($detail)? 'disabled="disabled"' : '')}}
                                    />
                                    <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Nopol:</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();" name="no_pol"
                                       class="form-control" placeholder="Nopol"
                                       value="{{old('no_pol',(isset($detail)? $detail->no_pol : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Jumlah:</label>
                                <input type="text"
                                       name="jumlah"
                                       class="form-control price input-currency" required>
                            </div>
                            <div class="form-group">
                                <label>Keterangan:</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();"
                                       name="keterangan"
                                       class="form-control" required>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="row">
                                        <div class="col-lg-12 ml-lg-auto">
                                            <button type="submit" class="btn btn-brand">Submit</button>
                                            <a data-url="{{url()->previous()}}"
                                               class="btn btn-secondary prevent-dialog"
                                               data-sw-title="Yakin Cancel?">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
